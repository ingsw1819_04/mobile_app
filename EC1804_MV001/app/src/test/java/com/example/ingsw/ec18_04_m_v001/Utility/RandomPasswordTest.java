package com.example.ingsw.ec18_04_m_v001.Utility;

import org.junit.Test;

import static com.example.ingsw.ec18_04_m_v001.Utility.Constants.ALPHANUMBERICSTRING;
import static com.example.ingsw.ec18_04_m_v001.Utility.Constants.DIMENSION;
import static org.junit.Assert.assertTrue;

public class RandomPasswordTest {

    @Test
    public void getAlphaNumericString() {
        assertTrue(DIMENSION > 0);
        StringBuilder stringBuilder = new StringBuilder(DIMENSION);
        for (int i = 0; i < DIMENSION; i++) {
            int index = (int) (ALPHANUMBERICSTRING.length() * Math.random());
            assertTrue(index > 0 && index <= ALPHANUMBERICSTRING.length());

            stringBuilder.append(ALPHANUMBERICSTRING.charAt(index));
        }
        assertTrue(!stringBuilder.toString().isEmpty());
    }
}