package com.example.ingsw.ec18_04_m_v001.Utility;

import org.junit.Test;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import static com.example.ingsw.ec18_04_m_v001.Utility.Constants.ORDER;
import static com.example.ingsw.ec18_04_m_v001.Utility.Html.ForgotPasswordHtml.getForgotPasswordContentMail;
import static com.example.ingsw.ec18_04_m_v001.Utility.Html.OrderPlacedHtml.getOrderPlacedContentMail;
import static com.example.ingsw.ec18_04_m_v001.Utility.Html.RefundHtml.getRepaymentContentMail;
import static com.example.ingsw.ec18_04_m_v001.Utility.Html.WelcomeHtml.WELCOME_PAGE;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class SendHTMLEmailTest {

    private static final String from = "ecommerceunina18@gmail.com";
    private static final String password = "(progettoPassword)";
    private static String to = "";
    private static String messageContent = "";
    private static String subject = "";
    private static final String choice = "0";

    @Test
    public void sendEmail() {
        to = "r.dilucrezia@gmail.com";
        switch (choice) {
            case "0":
                subject = "Benvenuto su ECommerce '18";
                messageContent = WELCOME_PAGE;
                break;
            case "1":
                subject = "ECommerce '18 - Nuova Password";
                messageContent = getForgotPasswordContentMail("babbano");
                break;
            case "2":
                subject = "ECommerce '18 - Ordine effettuato";
                messageContent = getOrderPlacedContentMail(ORDER.getAllOrders().get(0).getId());
                break;
            case "3":
                subject = "ECommerce '18 - Ordine cancellato";
                messageContent = getRepaymentContentMail(ORDER.getAllOrders().get(0).getId());
                break;
        }
        assertNotNull("messageContent: " + messageContent, messageContent);

        String host = "smtp.gmail.com";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(from, password);
                    }
                });
        assertNotNull("session: " + session, session);

        try {
            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress(from));

            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));

            message.setSubject(subject);

            message.setContent(
                    messageContent,
                    "text/html");

            Transport.send(message);
            System.out.println("Messaggio inviato con successo...");
        } catch (MessagingException e) {
            fail(e.toString());
        }
        assertTrue(true);
    }
}