package com.example.ingsw.ec18_04_m_v001.DAOImpl;

import com.example.ingsw.ec18_04_m_v001.DAO.OrderDetailDAO;
import com.example.ingsw.ec18_04_m_v001.DBClasses.OrderDetail;

import java.util.ArrayList;
import java.util.List;

public class OrderDetailDaoImpl implements OrderDetailDAO {

    private List<OrderDetail> orderDetails = new ArrayList<>();

    @Override
    public List<OrderDetail> getAllOrderDetails() {
        return orderDetails;
    }

    @Override
    public void updateOrderDetail(OrderDetail orderDetail) {
        for (OrderDetail od : orderDetails)
            if (od.equals(orderDetail)) {
                od.setQuantity(orderDetail.getQuantity());
                od.setSubTotal(orderDetail.getSubTotal());
                od.setUnitCost(orderDetail.getUnitCost());
            }
    }

    @Override
    public void deleteOrder(OrderDetail orderDetail) {
//        ignore
    }

    @Override
    public void addOrder(OrderDetail orderDetail) {
        for (OrderDetail od : orderDetails)
            if (od.equals(orderDetail))
                return;

        orderDetails.add(orderDetail);
    }
}
