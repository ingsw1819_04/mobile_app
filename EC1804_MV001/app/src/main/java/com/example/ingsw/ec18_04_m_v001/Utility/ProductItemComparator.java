package com.example.ingsw.ec18_04_m_v001.Utility;

import com.example.ingsw.ec18_04_m_v001.RecyclerView.ProductItem;

import java.util.Comparator;

public enum ProductItemComparator implements Comparator<ProductItem> {

    ID_SORT {
        public int compare(ProductItem p1, ProductItem p2) {
            return Integer.valueOf(p1.getId()).compareTo(p2.getId());
        }
    },
    NAME_SORT {
        public int compare(ProductItem p1, ProductItem p2) {
            return p1.getName().compareTo(p2.getName());
        }
    },
    PRICE_SORT {
        public int compare(ProductItem p1, ProductItem p2) {
            return Double.valueOf(p1.getPrice()).compareTo(p2.getPrice());
        }
    };

    public static Comparator<ProductItem> descending(final Comparator<ProductItem> other) {
        return (p1, p2) -> -1 * other.compare(p1, p2);
    }

    public static Comparator<ProductItem> getComparator(final ProductItemComparator... multipleOptions) {
        return (p1, p2) -> {
            for (ProductItemComparator option : multipleOptions) {
                int result = option.compare(p1, p2);
                if (result != 0) {
                    return result;
                }
            }
            return 0;
        };
    }
}
