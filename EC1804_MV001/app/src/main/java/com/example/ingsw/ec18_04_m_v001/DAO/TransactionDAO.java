package com.example.ingsw.ec18_04_m_v001.DAO;

import com.example.ingsw.ec18_04_m_v001.DBClasses.Transaction;

import java.util.List;

public interface TransactionDAO {
    public List<Transaction> getAllTransactions();

    public void updateTransaction(Transaction transaction);

    public void deleteTransaction(Transaction transaction);

    public void addTransaction(Transaction transaction);
}
