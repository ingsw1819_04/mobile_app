package com.example.ingsw.ec18_04_m_v001.Activity;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.ingsw.ec18_04_m_v001.DBClasses.Product;
import com.example.ingsw.ec18_04_m_v001.DBClasses.ProductCategory;
import com.example.ingsw.ec18_04_m_v001.DBClasses.User;
import com.example.ingsw.ec18_04_m_v001.R;
import com.example.ingsw.ec18_04_m_v001.RecyclerView.Adapter.ProductAdapter;
import com.example.ingsw.ec18_04_m_v001.RecyclerView.ProductItem;
import com.example.ingsw.ec18_04_m_v001.Utility.CRUDClass;
import com.example.ingsw.ec18_04_m_v001.Utility.Constants;
import com.example.ingsw.ec18_04_m_v001.Utility.ImageStorage;
import com.example.ingsw.ec18_04_m_v001.Utility.PreferenceData;
import com.example.ingsw.ec18_04_m_v001.Utility.ProductItemComparator;
import com.google.android.material.navigation.NavigationView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.example.ingsw.ec18_04_m_v001.Utility.ProductItemComparator.ID_SORT;
import static com.example.ingsw.ec18_04_m_v001.Utility.ProductItemComparator.NAME_SORT;
import static com.example.ingsw.ec18_04_m_v001.Utility.ProductItemComparator.PRICE_SORT;
import static com.example.ingsw.ec18_04_m_v001.Utility.ProductItemComparator.descending;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ProductAdapter.ProductViewHolder.OnNoteListner, SearchView.OnQueryTextListener {

    private static final int PICK_IMAGE = 1;
    private static final int LOGIN = 2;
    private List<ProductItem> productList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView.Adapter rcAdapter;
    private RecyclerView.LayoutManager rcLayoutManager;
    private ProgressBar loading;
    private Spinner category;
    private Spinner orderBy;
    private String selectedCategory = "Tutto";
    private String selectedOrderBy = "Ordina per";
    private Dialog myDialog;
    private SwipeRefreshLayout refreshLayout;
    private int sideMenuIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CRUDClass crudClass = new CRUDClass(this);
        crudClass.retrieveProducts();
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        loading = findViewById(R.id.progressBar);
        loading.setVisibility(View.VISIBLE);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        rcLayoutManager = new LinearLayoutManager(MainActivity.this);
        refreshLayout = findViewById(R.id.refreshLayout);

        refreshLayout.setColorSchemeResources(R.color.colorPrimaryDark);

        NavigationView navigationView = findViewById(R.id.nav_view);
        View view = navigationView.getHeaderView(0);
        TextView textView = view.findViewById(R.id.nav_mail);
        textView.setText(PreferenceData.getUserName(MainActivity.this));
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        // Hide order menu and show login/registration menu
        toggleMenuItems(navigationView);

        if (checkLoggedUser()) {
            crudClass.retrieveMainActivityData();
        } else {
            Constants.USER.addUser(new User(0, "guest@ecommerce.it", null, 0, null));
        }

        ImageView imageView = view.findViewById(R.id.imageView);

        if (ImageStorage.checkifImageExists(this, "profile_image")) {
            imageView.setImageURI(Uri.fromFile(ImageStorage.getImage(this, "profile_image.png")));
        }

        imageView.setOnClickListener(v -> {
            Intent intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.INTERNAL_CONTENT_URI);

            startActivityForResult(intent, PICK_IMAGE);
        });

        category = findViewById(R.id.category);
        orderBy = findViewById(R.id.orderBy);

        rcAdapter = new ProductAdapter(productList, MainActivity.this);
        recyclerView.setLayoutManager(rcLayoutManager);
        recyclerView.setAdapter(rcAdapter);

        new android.os.Handler().postDelayed(() -> {
            convertProductToProductItem();
            rcAdapter.notifyDataSetChanged();

            setCategorySpinner();
            setOrderBySpinner();
            loading.setVisibility(View.GONE);
        }, 3000);

        category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCategory = parent.getItemAtPosition(position).toString();
                if (selectedCategory.equals("Tutto")) {
                    convertProductToProductItem();
                } else {
                    productList.clear();
                    for (Product p : Constants.PRODUCT.getAllProducts()) {
                        if (p.getCategory().equals(selectedCategory)) {
                            productList.add(new ProductItem(p.getId(), p.getName(), p.getPrice(), p.getImage()));
                        }
                    }
                }
                orderListBySelection();
                rcAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // ignore
            }
        });

        orderBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedOrderBy = parent.getItemAtPosition(position).toString();
                orderListBySelection();
                rcAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // ignore
            }
        });

        refreshLayout.setOnRefreshListener(() -> {

            productList.clear();
            rcAdapter.notifyDataSetChanged();

            crudClass.retrieveProducts();

            new android.os.Handler().postDelayed(() -> {
                convertProductToProductItem();
                rcAdapter.notifyDataSetChanged();

                setCategorySpinner();
                setOrderBySpinner();
                refreshLayout.setRefreshing(false);
            }, 5000);
        });

        myDialog = new Dialog(this);
    }

    /**
     * Create a list for the category filter
     */
    private void setCategorySpinner() {
        ArrayList<String> categories = new ArrayList<>();
        categories.add("Tutto");
        for (ProductCategory pc : Constants.CATEGORY.getAllCategories()) {
            categories.add(pc.getCategory());
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, categories);
        category.setAdapter(arrayAdapter);
    }

    /**
     * Create a list for the order filter
     */
    private void setOrderBySpinner() {
        String[] orders = getResources().getStringArray(R.array.orders_array);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, orders);
        orderBy.setAdapter(arrayAdapter);
    }

    /**
     * Create a list for the products items
     */
    private void convertProductToProductItem() {
        productList.clear();
        for (Product p : Constants.PRODUCT.getAllProducts())
            productList.add(new ProductItem(p.getId(), p.getName(), p.getPrice(), p.getImage()));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);

        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchMenuItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        item.getItemId();

        // noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;

        if (id == R.id.nav_home) {
            new Intent(getApplicationContext(), MainActivity.class);
        } else if (id == R.id.nav_login) {
            intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivityForResult(intent, LOGIN);
        } else if (id == R.id.nav_profile) {
            intent = new Intent(getApplicationContext(), ProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_cart) {
            intent = new Intent(getApplicationContext(), CartActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_order) {
            intent = new Intent(getApplicationContext(), OrderActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_info) {
            sideMenuIndex = 1;
            myDialog.setContentView(R.layout.popup);
            TextView txtclose = myDialog.findViewById(R.id.txtclose);
            ImageView unina = myDialog.findViewById(R.id.unina);
            txtclose.setOnClickListener(v -> myDialog.dismiss());
            LinearLayout easterEgg = myDialog.findViewById(R.id.easterEgg);
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            easterEgg.setOnClickListener(v -> {
                if (sideMenuIndex < 3)
                    Toast.makeText(MainActivity.this, "Easter Egg tra " + (3 - sideMenuIndex) + "", Toast.LENGTH_SHORT).show();
                else if (sideMenuIndex == 3) {
                    Toast.makeText(MainActivity.this, "Scoperto un Easter Egg!", Toast.LENGTH_SHORT).show();
                    Intent intentEasterEggActivity = new Intent(this, EasterEggActivity.class);
                    intentEasterEggActivity.putExtra("VALUE", sideMenuIndex);
                    startActivity(intentEasterEggActivity);
                } else if (sideMenuIndex > 3 && sideMenuIndex < 9)
                    Toast.makeText(MainActivity.this, "Easter Egg tra " + (9 - sideMenuIndex) + "", Toast.LENGTH_SHORT).show();
                else if (sideMenuIndex == 9) {
                    Toast.makeText(MainActivity.this, "Scoperto l'altro Easter Egg!", Toast.LENGTH_SHORT).show();
                    Intent intentEasterEggActivity = new Intent(this, EasterEggActivity.class);
                    intentEasterEggActivity.putExtra("VALUE", sideMenuIndex);
                    startActivity(intentEasterEggActivity);
                } else
                    Toast.makeText(MainActivity.this, "Hai sbloccato tutti gli Easter Egg", Toast.LENGTH_SHORT).show();
                sideMenuIndex++;
            });

            unina.setOnClickListener(v -> {
                Intent intentUnina = new Intent();
                intentUnina.setAction(Intent.ACTION_VIEW);
                intentUnina.addCategory(Intent.CATEGORY_BROWSABLE);
                intentUnina.setData(Uri.parse("http://www.unina.it/home"));
                startActivity(intentUnina);
            });
            myDialog.show();
        } else if (id == R.id.nav_logout) {
            if (Constants.USER.getActualUser().getId() > 0) {
                PreferenceData.setUserName(MainActivity.this, "");
                TextView emailNav = findViewById(R.id.nav_mail);
                emailNav.setText("");

                emptyLoggedUserLists();

                // Hide order menu and show login/registration menu
                toggleMenuItems(findViewById(R.id.nav_view));

                Constants.USER.addUser(new User(0, "guest@ecommerce.it", null, 0, null));
                Toast.makeText(this, "Account disconnesso", Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(this, "Nessun utente da disconnettere", Toast.LENGTH_LONG).show();
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Clears all the lists of the logged in user
     */
    private void emptyLoggedUserLists() {
        Constants.USER.getAllUsers().clear();
        Constants.CUSTOMER.getAllCustomers().clear();
        Constants.ORDER.getAllOrders().clear();
        Constants.SHIPPING.getAllShippingInfos().clear();
        Constants.SHOPPING_CART.getAllShoppingCarts().clear();
        Constants.CART_ITEM.getAllCartItems().clear();
    }

    /**
     * Check if the user is logged in
     *
     * @return The validity of the check
     */
    private boolean checkLoggedUser() {
        return PreferenceData.getUserName(MainActivity.this).length() > 0;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == LOGIN) {

            String userEmail = data.getStringExtra("LOGIN_MAIL");
            TextView emailNav = findViewById(R.id.nav_mail);
            emailNav.setText(userEmail);

            // Hide order menu and show login/registration menu
            toggleMenuItems(findViewById(R.id.nav_view));
        }

        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            Uri selectedImage = data.getData();

            try {
                ImageStorage.saveInternalStorage(this, MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage), "profile_image");
            } catch (IOException e) {
                e.printStackTrace();
            }

            NavigationView naview = findViewById(R.id.nav_view);
            ImageView imageView = naview.getHeaderView(0).findViewById(R.id.imageView);
            imageView.setImageURI(selectedImage);
        }
    }

    /**
     * Hide or show menu items
     *
     * @param nav NavigationView of the items to show or hide
     */
    private void toggleMenuItems(NavigationView nav) {
        nav.getMenu().getItem(1).setVisible(!checkLoggedUser());
        nav.getMenu().getItem(2).setVisible(checkLoggedUser());
        nav.getMenu().getItem(4).setVisible(checkLoggedUser());
    }

    @Override
    public void onNoteClick(int position) {
        Intent intentProductActivity = new Intent(this, ProductActivity.class);
        intentProductActivity.putExtra("PRODUCT_ID", productList.get(position).getId());
        startActivity(intentProductActivity);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        productList.clear();
        for (Product p : Constants.PRODUCT.getAllProducts()) {
            if (containsIgnoreCase(p.getName(), newText) && (selectedCategory.equals("Tutto") || selectedCategory.equals(p.getCategory()))) {
                productList.add(new ProductItem(p.getId(), p.getName(), p.getPrice(), p.getImage()));
            }
        }

        orderListBySelection();
        rcAdapter.notifyDataSetChanged();
        return true;
    }

    /**
     * Method to check if there is a sub-sequence within a string, regardless of how the string is written
     *
     * @param str       Original string
     * @param subString Sub-sequence for research
     * @return Whether or not the indicated sub-sequence is present
     */
    private boolean containsIgnoreCase(String str, String subString) {
        return str.toLowerCase().contains(subString.toLowerCase());
    }

    /**
     * Method for sorting the list based on the methodology
     */
    private void orderListBySelection() {
        switch (selectedOrderBy) {
            case "Ordina per":
                Collections.sort(productList, ProductItemComparator.getComparator(ID_SORT));
                break;
            case "Nome A-Z":
                Collections.sort(productList, ProductItemComparator.getComparator(NAME_SORT));
                break;
            case "Nome Z-A":
                Collections.sort(productList, descending(ProductItemComparator.getComparator(NAME_SORT)));
                break;
            case "Prezzo cresc":
                Collections.sort(productList, ProductItemComparator.getComparator(PRICE_SORT));
                break;
            case "Prezzo decresc":
                Collections.sort(productList, descending(ProductItemComparator.getComparator(PRICE_SORT)));
                break;
        }
    }
}
