package com.example.ingsw.ec18_04_m_v001.Activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Patterns;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ingsw.ec18_04_m_v001.Connection.DBConnection;
import com.example.ingsw.ec18_04_m_v001.R;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static java.util.Calendar.DATE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;

public class RegistrationActivity extends AppCompatActivity {

    private static final int REGISTRATION_REQUEST = 123;
    //    Ui references
    private DatePickerDialog datePickerDialog;
    private EditText birthdateEditText;
    private Button registerButton;
    private EditText usernameEditText;
    private EditText passwordEditText;
    private EditText passwordEditText2;
    private EditText nameEditText;
    private EditText surnameEditText;
    private EditText addressEditText;
    private EditText cityEditText;
    private EditText capEditText;
    private AutoCompleteTextView regionACTV;
    private RadioGroup sexRadioGroup;
    private boolean emailPresence = false;

    private static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        usernameEditText = findViewById(R.id.username);
        passwordEditText = findViewById(R.id.password);
        passwordEditText2 = findViewById(R.id.password2);
        nameEditText = findViewById(R.id.name);
        surnameEditText = findViewById(R.id.surname);
        addressEditText = findViewById(R.id.address);
        regionACTV = findViewById(R.id.region);
        cityEditText = findViewById(R.id.city);
        capEditText = findViewById(R.id.cap);
        sexRadioGroup = findViewById(R.id.sexRadioGroup);
        registerButton = findViewById(R.id.register);
        birthdateEditText = findViewById(R.id.birthdate);
        birthdateEditText.setShowSoftInputOnFocus(false);
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        birthdateEditText.setText(dateFormat.format(Calendar.getInstance().getTime()));
        String[] regions = getResources().getStringArray(R.array.regions_array);
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, regions);
        regionACTV.setAdapter(adapter);

        prepareDatePickerDialog();

        birthdateEditText.setOnTouchListener((v, event) -> {
            birthdateEditText.setInputType(InputType.TYPE_NULL);
            datePickerDialog.show();
            return true;
        });

        birthdateEditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                birthdateEditText.setInputType(InputType.TYPE_NULL);
                datePickerDialog.show();
            }
        });

        birthdateEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isDateValid(birthdateEditText.getText().toString())) {
                    birthdateEditText.setFocusable(true);
                    birthdateEditText.setError(getString(R.string.error_age_restriction));
                    birthdateEditText.requestFocus();
                } else {
                    birthdateEditText.setError(null);
                }
                checkRequiredFields();
            }
        });

        usernameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                String email = usernameEditText.getText().toString();
                CheckEmailExistence checkEmailExistence = new CheckEmailExistence();
                checkEmailExistence.execute(email);
                new android.os.Handler().postDelayed(() -> {
                    if (!isEmailValid(email)) {
                        usernameEditText.setError(getString(R.string.error_invalid_email));
                    } else if (emailPresence) {
                        usernameEditText.setError(getString(R.string.error_existing_email));
                    }
                }, 100);
                checkRequiredFields();
            }
        });

        passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isPasswordValid(passwordEditText.getText().toString())) {
                    passwordEditText.setError(getString(R.string.error_invalid_password));
                }
                checkRequiredFields();
            }
        });

        passwordEditText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!passwordEditText.getText().toString().equals(passwordEditText2.getText().toString())) {
                    passwordEditText2.setError(getString(R.string.error_nomatch_password));
                }
                checkRequiredFields();
            }
        });

        nameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isFieldValid(nameEditText.getText().toString())) {
                    nameEditText.setError(getString(R.string.error_field_required));
                }
                checkRequiredFields();
            }
        });

        surnameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isFieldValid(surnameEditText.getText().toString())) {
                    surnameEditText.setError(getString(R.string.error_field_required));
                }
                checkRequiredFields();
            }
        });

        addressEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isFieldValid(addressEditText.getText().toString())) {
                    addressEditText.setError(getString(R.string.error_field_required));
                }
                checkRequiredFields();
            }
        });

        cityEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isFieldValid(cityEditText.getText().toString())) {
                    cityEditText.setError(getString(R.string.error_field_required));
                }
                checkRequiredFields();
            }
        });

        regionACTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isRegionValid(regionACTV.getText().toString())) {
                    regionACTV.setError(getString(R.string.error_invalid_region));
                } else {
                    regionACTV.setError(null);
                }
                checkRequiredFields();
            }
        });

        capEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                String cap = capEditText.getText().toString();
                if (!isCapValid(cap)) {
                    capEditText.setError(getString(R.string.error_invalid_cap));
                } else if (cap.length() < 5) {
                    capEditText.setError(getString(R.string.error_short_cap));
                }
                checkRequiredFields();
            }
        });

        sexRadioGroup.setOnCheckedChangeListener((group, checkedId) -> checkRequiredFields());

        registerButton.setOnClickListener(v -> {
            int id = sexRadioGroup.getCheckedRadioButtonId();
            RadioButton selectedRadButton = findViewById(id);
            Intent intentConfirmRegistrationActivity = new Intent(this, ConfirmRegistrationActivity.class);
            intentConfirmRegistrationActivity.putExtra("EMAIL", usernameEditText.getText().toString());
            intentConfirmRegistrationActivity.putExtra("PASSWORD", passwordEditText.getText().toString());
            intentConfirmRegistrationActivity.putExtra("ADDRESS", convertToTitleCaseIteratingChars(addressEditText.getText().toString()));
            intentConfirmRegistrationActivity.putExtra("CAP", capEditText.getText().toString());
            intentConfirmRegistrationActivity.putExtra("BIRTHDATE", birthdateEditText.getText().toString());
            intentConfirmRegistrationActivity.putExtra("NAME", nameEditText.getText().toString());
            intentConfirmRegistrationActivity.putExtra("SURNAME", surnameEditText.getText().toString());
            intentConfirmRegistrationActivity.putExtra("CITY", convertToTitleCaseIteratingChars(cityEditText.getText().toString()));
            intentConfirmRegistrationActivity.putExtra("REGION", regionACTV.getText().toString());
            intentConfirmRegistrationActivity.putExtra("SEX", selectedRadButton.getText().toString());
            startActivityForResult(intentConfirmRegistrationActivity, REGISTRATION_REQUEST);
        });
    }

    /**
     * Allows you to convert edit the input string by capitalizing the initial letters of each word
     * example: input:  "Nel mezzo del cammin di nostra vita"
     * output: "Nel Mezzo Del Cammin Di Nostra Vita"
     *
     * @param text String to be edited
     * @return The modified string
     */
    private String convertToTitleCaseIteratingChars(String text) {
        if (text == null || text.isEmpty()) {
            return text;
        }

        StringBuilder converted = new StringBuilder();

        boolean convertNext = true;
        for (char ch : text.toCharArray()) {
            if (Character.isSpaceChar(ch)) {
                convertNext = true;
            } else if (convertNext) {
                ch = Character.toTitleCase(ch);
                convertNext = false;
            } else {
                ch = Character.toLowerCase(ch);
            }
            converted.append(ch);
        }

        return converted.toString();
    }

    /**
     * Check that the email is valid
     *
     * @param email String to verify
     * @return The verification on the string
     */
    private boolean isEmailValid(String email) {
        return (!email.isEmpty() && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    /**
     * Check that the password is valid
     *
     * @param password String to verify
     * @return The verification on the string
     */
    private boolean isPasswordValid(String password) {
        return password.length() > 5;
    }

    /**
     * Check that the cap is valid
     *
     * @param cap String to verify
     * @return The verification on the string
     */
    private boolean isCapValid(String cap) {
        int value;
        try {
            value = Integer.parseInt(cap);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return value > 9 && value < 98169;
    }

    /**
     * Check that the region is valid
     *
     * @param region String to verify
     * @return The verification on the string
     */
    private boolean isRegionValid(String region) {
        return (region.equalsIgnoreCase("Abruzzo") || region.equalsIgnoreCase("Basilicata") || region.equalsIgnoreCase("Calabria")
                || region.equalsIgnoreCase("Campania") || region.equalsIgnoreCase("Emilia-Romagna") || region.equalsIgnoreCase("Friuli-Venezia Giulia")
                || region.equalsIgnoreCase("Lazio") || region.equalsIgnoreCase("Liguria") || region.equalsIgnoreCase("Lombardia")
                || region.equalsIgnoreCase("Marche") || region.equalsIgnoreCase("Molise") || region.equalsIgnoreCase("Piemonte")
                || region.equalsIgnoreCase("Puglia") || region.equalsIgnoreCase("Sardegna") || region.equalsIgnoreCase("Sicilia")
                || region.equalsIgnoreCase("Toscana") || region.equalsIgnoreCase("Trentino-Alto Adige") || region.equalsIgnoreCase("Umbria")
                || region.equalsIgnoreCase("Veneto") || region.equalsIgnoreCase("Valle D\'Aosta"));
    }

    /**
     * Check that the field isn't empty
     *
     * @param field Field to verify
     * @return Field verification
     */
    private boolean isFieldValid(String field) {
        return !field.isEmpty();
    }

    /**
     * Check that the date is a valid date
     *
     * @param date Date to be verified
     * @return Field verification
     */
    private boolean isDateValid(String date) {
        @SuppressLint("SimpleDateFormat")
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date datebirth;
        try {
            datebirth = format.parse(date);
        } catch (ParseException pe) {
            return false;
        }
        return (getYears(Calendar.getInstance().getTime(), datebirth) > 17);
    }

    /**
     * Check that the current year and date are different
     *
     * @param today Current date
     * @param date  Date of birth
     * @return The difference between the current date and the birth date
     */
    private int getYears(Date today, Date date) {
        Calendar calendarDate = getCalendar(date);
        Calendar calendarToday = getCalendar(today);
        int diff = calendarToday.get(YEAR) - calendarDate.get(YEAR);
        if (calendarDate.get(MONTH) > calendarToday.get(MONTH) ||
                (calendarDate.get(MONTH) == calendarToday.get(MONTH) && calendarDate.get(DATE) > calendarToday.get(DATE))) {
            diff--;
        }
        return diff;
    }

    /**
     * Organize the datepicker management dialog
     */
    @SuppressLint("SetTextI18n")
    private void prepareDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();

        datePickerDialog = new DatePickerDialog(this, (view, year, monthOfYear, dayOfMonth) -> {
            ++monthOfYear;
            birthdateEditText.setText(year + "-" + monthOfYear + "-" + dayOfMonth);
            datePickerDialog.dismiss();
        }, calendar.get(YEAR), calendar.get(MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * Check if there are mandatory fields
     */
    private void checkRequiredFields() {
        registerButton.setEnabled(noEmptyFields() && noErrorFields() && isDateValid(birthdateEditText.getText().toString()));
    }

    /**
     * Check that there are no errors within the fields
     *
     * @return The status of the verification
     */
    private boolean noErrorFields() {
        return usernameEditText.getError() == null &&
                passwordEditText.getError() == null &&
                passwordEditText2.getError() == null &&
                nameEditText.getError() == null &&
                surnameEditText.getError() == null &&
                addressEditText.getError() == null &&
                regionACTV.getError() == null &&
                cityEditText.getError() == null &&
                capEditText.getError() == null &&
                birthdateEditText.getError() == null;
    }

    /**
     * Check that there are no empty fields in the form
     *
     * @return The status of the verification
     */
    private boolean noEmptyFields() {
        return !usernameEditText.getText().toString().isEmpty() &&
                !passwordEditText.getText().toString().isEmpty() &&
                !passwordEditText2.getText().toString().isEmpty() &&
                !nameEditText.getText().toString().isEmpty() &&
                !surnameEditText.getText().toString().isEmpty() &&
                !addressEditText.getText().toString().isEmpty() &&
                !cityEditText.getText().toString().isEmpty() &&
                !regionACTV.getText().toString().isEmpty() &&
                !capEditText.getText().toString().isEmpty() &&
                !birthdateEditText.getText().toString().isEmpty() &&
                sexRadioGroup.getCheckedRadioButtonId() != -1;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REGISTRATION_REQUEST) {
                finish();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class CheckEmailExistence extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            int index = 0;
            try {
                Connection connection = DBConnection.getConnection();

                PreparedStatement preparedStatement = connection.prepareStatement("SELECT USERNAME FROM Users WHERE USERNAME = ?");
                preparedStatement.setString(1, params[0]);
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.last();
                index = resultSet.getRow();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return index > 0;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            emailPresence = aBoolean;
        }
    }
}
