package com.example.ingsw.ec18_04_m_v001.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ingsw.ec18_04_m_v001.R;
import com.example.ingsw.ec18_04_m_v001.Utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

public class PaymentDetails extends AppCompatActivity {

    private TextView id;
    private TextView total;
    private TextView status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);

        id = findViewById(R.id.id);
        total = findViewById(R.id.total);
        status = findViewById(R.id.status);

        Intent intent = getIntent();

        try {
            JSONObject jsonObject = new JSONObject(intent.getStringExtra("PaymentDetails"));
            showDetails(jsonObject.getJSONObject("response"), intent.getStringExtra("PaymentTotal"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * It allows you to view information about the payment
     *
     * @param response      JSON object from which to take the transaction id
     * @param paymentAmount Total value to be paid
     */
    private void showDetails(JSONObject response, String paymentAmount) {
        try {
            Constants.PAYPAL_TRANSATION_ID = response.getString("id");
            id.setText(Constants.PAYPAL_TRANSATION_ID);
            status.setText(response.getString("state"));
            total.setText("€ " + paymentAmount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
