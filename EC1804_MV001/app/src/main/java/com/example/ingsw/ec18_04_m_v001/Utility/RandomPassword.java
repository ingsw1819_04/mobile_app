package com.example.ingsw.ec18_04_m_v001.Utility;

import static com.example.ingsw.ec18_04_m_v001.Utility.Constants.ALPHANUMBERICSTRING;
import static com.example.ingsw.ec18_04_m_v001.Utility.Constants.DIMENSION;

public class RandomPassword {

    /**
     * Generate an alphanumeric string that will be sent via email to the account that requested
     * the change
     *
     * @return a random password
     */
    public static String getAlphaNumericString() {
        // create StringBuffer size of AlphaNumericString
        StringBuilder stringBuilder = new StringBuilder(DIMENSION);

        for (int i = 0; i < DIMENSION; i++) {
            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index = (int) (ALPHANUMBERICSTRING.length() * Math.random());

            // add Character one by one in end of stringBuilder
            stringBuilder.append(ALPHANUMBERICSTRING.charAt(index));
        }
        return stringBuilder.toString();
    }
}
