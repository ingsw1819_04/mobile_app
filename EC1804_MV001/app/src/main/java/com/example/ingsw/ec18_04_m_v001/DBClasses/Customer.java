package com.example.ingsw.ec18_04_m_v001.DBClasses;

import java.sql.Date;

public class Customer {

    private String email;
    private String name;
    private String surname;
    private Date dateofbirth;
    private String sex;
    private String region;
    private String address;
    private String city;
    private int cap;

    public Customer(String email, String name, String surname, Date dateofbirth, String sex, String region, String address, String city, int cap) {
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.dateofbirth = dateofbirth;
        this.sex = sex;
        this.region = region;
        this.address = address;
        this.city = city;
        this.cap = cap;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getDateofbirth() {
        return dateofbirth;
    }

    public void setDateofbirth(Date dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getCap() {
        return cap;
    }

    public void setCap(int cap) {
        this.cap = cap;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof Customer))
            return false;

        Customer c = (Customer) obj;
        return email.equals(c.email);
    }
}
