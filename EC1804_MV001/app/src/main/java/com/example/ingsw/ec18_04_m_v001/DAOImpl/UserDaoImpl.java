package com.example.ingsw.ec18_04_m_v001.DAOImpl;

import com.example.ingsw.ec18_04_m_v001.DAO.UserDAO;
import com.example.ingsw.ec18_04_m_v001.DBClasses.User;

import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDAO {

    private List<User> users = new ArrayList<>();

    @Override
    public List<User> getAllUsers() {
        return users;
    }

    public User getActualUser() {
        return users.get(0);
    }

    @Override
    public void updateUser(User user) {
        for (User u : users)
            if (u.equals(user)) {
                u.setPasswordHash(user.getPasswordHash());
                u.setLoginStatus(user.getLoginStatus());
                u.setRegisterDate(user.getRegisterDate());
            }
    }

    @Override
    public void deleteUser(User user) {
        users.remove(user);
    }

    @Override
    public void addUser(User user) {
        for (User u : users)
            if (u.equals(user))
                return;

        users.add(user);
    }
}
