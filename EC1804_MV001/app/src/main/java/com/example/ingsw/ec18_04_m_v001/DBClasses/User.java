package com.example.ingsw.ec18_04_m_v001.DBClasses;

import java.sql.Timestamp;

public class User {

    private int id;
    private String username;
    private String passwordHash;
    private int loginStatus;
    private Timestamp registerDate;

    public User(int id, String username, String passwordHash, int loginStatus, Timestamp registerDate) {
        this.id = id;
        this.username = username;
        this.passwordHash = passwordHash;
        this.loginStatus = loginStatus;
        this.registerDate = registerDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public int getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(int loginStatus) {
        this.loginStatus = loginStatus;
    }

    public Timestamp getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Timestamp registerDate) {
        this.registerDate = registerDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof User))
            return false;
        User u = (User) obj;

        return id == u.id && username.equals(u.username);
    }
}
