package com.example.ingsw.ec18_04_m_v001.DBClasses;

import java.sql.Blob;

public class CartItem {

    private int productId;
    private String name;
    private int quantity;
    private double unitCost;
    private Blob image;

    public CartItem(int productId, String name, int quantity, double unitCost, Blob image) {
        this.productId = productId;
        this.name = name;
        this.quantity = quantity;
        this.unitCost = unitCost;
        this.image = image;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(double unitCost) {
        this.unitCost = unitCost;
    }

    public Blob getImage() {
        return image;
    }

    public void setImage(Blob image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof CartItem))
            return false;

        CartItem ci = (CartItem) obj;
        return productId == ci.productId && name.equals(ci.name);
    }

    @Override
    public String toString() {
        return "CartItem{" +
                "productId=" + productId +
                ", name='" + name + '\'' +
                ", quantity=" + quantity +
                ", unitCost=" + unitCost +
                ", image=" + image +
                '}';
    }
}
