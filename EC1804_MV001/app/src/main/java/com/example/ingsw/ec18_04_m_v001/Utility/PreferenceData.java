package com.example.ingsw.ec18_04_m_v001.Utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import static com.example.ingsw.ec18_04_m_v001.Utility.Constants.PREF_USER_NAME;

public class PreferenceData {

    /**
     * Get all saved session information
     *
     * @param context information about an application environment
     * @return the information of the session
     */
    static SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Save the username of the session
     *
     * @param context  information about an application environment
     * @param userName related to the session
     */
    public static void setUserName(Context context, String userName) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.commit();
    }

    /**
     * Get the saved session username
     *
     * @param context information about an application environment
     * @return username of the session
     */
    public static String getUserName(Context context) {
        return getSharedPreferences(context).getString(PREF_USER_NAME, "");
    }
}