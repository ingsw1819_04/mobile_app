package com.example.ingsw.ec18_04_m_v001.Activity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ingsw.ec18_04_m_v001.DBClasses.Product;
import com.example.ingsw.ec18_04_m_v001.DBClasses.ShoppingCart;
import com.example.ingsw.ec18_04_m_v001.R;
import com.example.ingsw.ec18_04_m_v001.Utility.CRUDClass;
import com.example.ingsw.ec18_04_m_v001.Utility.Constants;

import java.sql.Blob;
import java.sql.SQLException;

public class ProductActivity extends AppCompatActivity {

    private ImageView productImage;
    private ImageView addToCart;
    private TextView name;
    private TextView quantity;
    private TextView description;
    private TextView productPrice;
    private Button decrease;
    private Button increase;
    private Product selectedProduct;
    private int shopId = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        productImage = findViewById(R.id.product_image);
        addToCart = findViewById(R.id.add_to_cart);
        name = findViewById(R.id.product_name);
        quantity = findViewById(R.id.product_quantity);
        description = findViewById(R.id.product_description);
        productPrice = findViewById(R.id.product_price);
        decrease = findViewById(R.id.decrease);
        increase = findViewById(R.id.increase);

        Bundle extras = getIntent().getExtras();
        selectedProduct = Constants.PRODUCT.getProduct(extras.getInt("PRODUCT_ID"));

        setProductData();

        addToCart.setOnClickListener(v -> {
            int userID = Constants.USER.getActualUser().getId();
            int productID = selectedProduct.getId();
            int quantityValue = Integer.valueOf(quantity.getText().toString());
            if (userID > 0) {
                CRUDClass crudClass = new CRUDClass(ProductActivity.this);
                crudClass.updateShoppingCart(selectedProduct.getId(), quantityValue, userID, 1, userID > 0);
            }
            Constants.SHOPPING_CART.addShoppingCart(new ShoppingCart(shopId, userID, productID, quantityValue));
            finish();
        });

        decrease.setOnClickListener(v -> decreaseQuantity(Integer.valueOf(quantity.getText().toString())));

        increase.setOnClickListener(v -> increaseQuantity(Integer.valueOf(quantity.getText().toString())));
    }

    /**
     * The method sets information about the product data
     */
    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    private void setProductData() {
        setProductImage();
        name.setText(selectedProduct.getName());
        quantity.setText("1");
        description.setText(selectedProduct.getDescription());
        productPrice.setText("€ " + String.format("%.2f", selectedProduct.getPrice()).replace(",", "."));
    }

    /**
     * The method sets information about the product image
     */
    private void setProductImage() {
        Blob image = selectedProduct.getImage();
        byte[] byteArray = null;
        try {
            byteArray = image.getBytes(1, (int) (image.length()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        productImage.setImageBitmap(bitmap);
    }

    /**
     * Increase the quantity of the product
     *
     * @param value New quantity to set
     */
    private void increaseQuantity(int value) {
        if (value < selectedProduct.getQuantity())
            quantity.setText("" + ++value);
    }

    /**
     * Decrease the quantity of the product
     *
     * @param value New quantity to set
     */
    private void decreaseQuantity(int value) {
        if (value > 1)
            quantity.setText("" + --value);
    }
}
