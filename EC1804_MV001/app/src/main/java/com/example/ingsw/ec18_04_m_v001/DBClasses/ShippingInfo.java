package com.example.ingsw.ec18_04_m_v001.DBClasses;

public class ShippingInfo {

    private int id;
    private String type;
    private double cost;
    private String regionName;

    public ShippingInfo(int id, String type, double cost, String regionName) {
        this.id = id;
        this.type = type;
        this.cost = cost;
        this.regionName = regionName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof ShippingInfo))
            return false;

        ShippingInfo si = (ShippingInfo) obj;
        return id == si.id && regionName.equals(si.regionName);
    }
}
