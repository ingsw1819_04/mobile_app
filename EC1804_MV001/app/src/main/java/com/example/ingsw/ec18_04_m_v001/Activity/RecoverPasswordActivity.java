package com.example.ingsw.ec18_04_m_v001.Activity;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ingsw.ec18_04_m_v001.Connection.DBConnection;
import com.example.ingsw.ec18_04_m_v001.R;
import com.example.ingsw.ec18_04_m_v001.Utility.RandomPassword;
import com.example.ingsw.ec18_04_m_v001.Utility.SendHTMLEmail;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class RecoverPasswordActivity extends AppCompatActivity {

    private static boolean emailExistence = false;
    private static boolean updatedPassword = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recover_password);

        final Button recoverButton = findViewById(R.id.login);
        final EditText usernameEditText = findViewById(R.id.username);

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                recoverButton.setEnabled(validateEmail(usernameEditText));
                CheckEmailExistence checkEmailExistence = new CheckEmailExistence();
                checkEmailExistence.execute("SELECT USERNAME FROM Users WHERE USERNAME = ?", usernameEditText.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                // ignore
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);

        recoverButton.setOnClickListener(v -> {
            final String username = usernameEditText.getText().toString();
            final String newPassword = RandomPassword.getAlphaNumericString();
            if (emailExistence) {
                UpdatePassword updatePassword = new UpdatePassword();
                updatePassword.execute(newPassword, username);
                new android.os.Handler().postDelayed(() -> {
                    if (updatedPassword) {
                        SendMailTask sendMailTask = new SendMailTask();
                        sendMailTask.execute(username, newPassword, "1");
                        Toast.makeText(RecoverPasswordActivity.this, "Password inviata", Toast.LENGTH_SHORT).show();
                    }
                    finish();
                }, 500);
            } else {
                Toast.makeText(RecoverPasswordActivity.this, "Email non registrata", Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Check if the email entered is a valid email
     *
     * @param usernameEditText Field that must not be empty in order to recover the password
     * @return Email validation
     */
    public boolean validateEmail(EditText usernameEditText) {
        String emailInput = usernameEditText.getText().toString().trim();

        if (emailInput.isEmpty()) {
            usernameEditText.setError("Il campo non può essere vuoto");
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()) {
            usernameEditText.setError("Inserire un indirizzo valido");
            return false;
        } else {
            usernameEditText.setError(null);
            return true;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class UpdatePassword extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... parameters) {
            int index = 0;
            try {
                Connection connection = DBConnection.getConnection();

                PreparedStatement preparedStatement = connection.prepareStatement("UPDATE Users SET PASSWORD_HASH = ? WHERE USERNAME = ?");
                preparedStatement.setString(1, parameters[0]);
                preparedStatement.setString(2, parameters[1]);
                index = preparedStatement.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            }
            updatedPassword = index > 0;
            return index > 0;
        }

        @Override
        protected void onPostExecute(Boolean value) {
            updatedPassword = value;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class SendMailTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            SendHTMLEmail.sendEmail(strings[0], strings[1], strings[2]);
            return "OK";
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class CheckEmailExistence extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            int index = 0;
            try {
                Connection connection = DBConnection.getConnection();

                PreparedStatement preparedStatement = connection.prepareStatement(params[0]);
                preparedStatement.setString(1, params[1]);
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.last();
                index = resultSet.getRow();
            } catch (Exception e) {
                e.printStackTrace();
            }
            emailExistence = index != 0;
            return null;
        }
    }
}
