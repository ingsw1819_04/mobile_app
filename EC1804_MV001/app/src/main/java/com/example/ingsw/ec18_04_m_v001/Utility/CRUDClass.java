package com.example.ingsw.ec18_04_m_v001.Utility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.widget.Toast;

import com.example.ingsw.ec18_04_m_v001.Connection.DBConnection;
import com.example.ingsw.ec18_04_m_v001.DBClasses.CartItem;
import com.example.ingsw.ec18_04_m_v001.DBClasses.Customer;
import com.example.ingsw.ec18_04_m_v001.DBClasses.Order;
import com.example.ingsw.ec18_04_m_v001.DBClasses.OrderDetail;
import com.example.ingsw.ec18_04_m_v001.DBClasses.Product;
import com.example.ingsw.ec18_04_m_v001.DBClasses.ProductCategory;
import com.example.ingsw.ec18_04_m_v001.DBClasses.ShippingInfo;
import com.example.ingsw.ec18_04_m_v001.DBClasses.ShoppingCart;
import com.example.ingsw.ec18_04_m_v001.DBClasses.Transaction;
import com.example.ingsw.ec18_04_m_v001.DBClasses.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import static android.os.Process.THREAD_PRIORITY_BACKGROUND;
import static android.os.Process.THREAD_PRIORITY_MORE_FAVORABLE;

public class CRUDClass {

    private Activity activity;

    final Runnable runnable = () -> {
        RetrieveLoggedUserOrders retrieveLoggedUserOrders = new RetrieveLoggedUserOrders();
        retrieveLoggedUserOrders.execute(false);

        RetrieveLoggedUserData retrieveLoggedUserData = new RetrieveLoggedUserData();
        retrieveLoggedUserData.execute();

        RetrieveShippingData retrieveShippingData = new RetrieveShippingData();
        retrieveShippingData.execute();

        retrieveTransactions();
    };

    public CRUDClass(Activity activity) {
        this.activity = activity;
    }

    /**
     * It allows you to recover products and categories
     */
    public void retrieveProducts() {
        RetrieveProducts retrieveProducts = new RetrieveProducts();
        retrieveProducts.execute();

        RetrieveCategories retrieveCategories = new RetrieveCategories();
        retrieveCategories.execute();
    }

    /**
     * It allows to recover the data related to the MainActivity
     */
    public void retrieveMainActivityData() {
        RetrieveLoggedUser retrieveLoggedUser = new RetrieveLoggedUser();
        retrieveLoggedUser.execute();

        RetrieveShoppingCart retrieveShoppingCart = new RetrieveShoppingCart();
        retrieveShoppingCart.execute();

        new android.os.Handler().postDelayed(runnable, 500);
    }

    /**
     * It allows to recover the data of the logged in user
     */
    public void retrieveLoggedUserData() {
        RetrieveLoggedUser retrieveLoggedUser = new RetrieveLoggedUser();
        retrieveLoggedUser.execute();

        new android.os.Handler().postDelayed(runnable, 500);
    }

    /**
     * Recover cart data
     */
    public void retrieveShoppingCart() {
        RetrieveShoppingCart retrieveShoppingCart = new RetrieveShoppingCart();
        retrieveShoppingCart.execute();
    }

    /**
     * Retrieve the order data of the logged in user
     */
    private void retrieveLoggedUserOrders() {
        RetrieveLoggedUserOrders retrieveLoggedUserOrders = new RetrieveLoggedUserOrders();
        retrieveLoggedUserOrders.execute();
    }

    /**
     * Retrieve the last order data of the logged in user
     */
    public void retrieveLastOrder() {
        RetrieveLastOrder retrieveLastOrder = new RetrieveLastOrder();
        retrieveLastOrder.execute();
    }

    /**
     * Retrieve the order details of the logged in user
     */
    public void retrieveOrderDetails() {
        RetrieveOrderDetails retrieveOrderDetails = new RetrieveOrderDetails();
        retrieveOrderDetails.execute();
    }

    /**
     * Recover transaction data
     */
    private void retrieveTransactions() {
        RetrieveTransactions retrieveTransactions = new RetrieveTransactions();
        retrieveTransactions.execute();
    }

    /**
     * Delete an item from the cart
     *
     * @param productID ID of the product to be deleted
     */
    public void deleteShoppingCartItem(int productID) {
        DeleteShoppingCartItem deleteShoppingCartItem = new DeleteShoppingCartItem();
        deleteShoppingCartItem.execute(productID);
    }

    /**
     * Delete the cart
     */
    private void deleteShoppingCart() {
        DeleteShoppingCart deleteShoppingCart = new DeleteShoppingCart();
        deleteShoppingCart.execute();
    }

    /**
     * Allows you to insert or update items inside the cart
     *
     * @param productID   ID of the product to be updated
     * @param quantity    Number of objects to be inserted
     * @param userID      ID of the user to be assigned the update
     * @param choice      Type of message to be displayed
     * @param showMessage Show or not the message
     */
    public void updateShoppingCart(int productID, int quantity, int userID, int choice, boolean showMessage) {
        updateShoppingCart updateShoppingCart = new updateShoppingCart();
        updateShoppingCart.execute(productID, quantity, userID, choice, showMessage ? 1 : 0);
    }

    /**
     * Create a new order
     *
     * @param name       User name
     * @param surname    User surname
     * @param userID     User ID
     * @param shippingID Shipment ID
     */
    public void insertOrder(String name, String surname, int userID, int shippingID) {
        InsertOrder insertOrder = new InsertOrder();
        insertOrder.execute(name, surname, String.valueOf(userID), String.valueOf(shippingID));
    }

    /**
     * Create a new order detail
     *
     * @param orderID     Order ID
     * @param productID   ID of the product to be inserted
     * @param productName Product name
     * @param quantity    Number of items to insert
     * @param price       Product price
     */
    private void insertOrderDetails(int orderID, int productID, String productName, int quantity, double price) {
        InsertOrderDetails insertOrderDetails = new InsertOrderDetails();
        insertOrderDetails.execute(String.valueOf(orderID), String.valueOf(productID), productName, String.valueOf(quantity), String.valueOf(price));
    }

    /**
     * Allows you to insert or update the transactions
     *
     * @param orderID Order ID
     * @param choice  Type of transaction
     */
    public void updateTransactions(int orderID, int choice) {
        updateTransactions updateTransactions = new updateTransactions();
        updateTransactions.execute(orderID, choice);
    }

    /**
     * Allows you to cancel an order
     *
     * @param orderID Order ID
     */
    public void cancelOrder(int orderID) {
        CancelOrder cancelOrder = new CancelOrder();
        cancelOrder.execute(orderID);
    }

    /**
     * It allows you to send an email depending on the operation performed
     *
     * @param email   Address to which to send the email
     * @param message Body of the mail
     * @param choice  Type of mail to send
     */
    public void sendEmail(String email, String message, String choice) {
        SendEmail sendEmail = new SendEmail();
        sendEmail.execute(email, message, choice);
    }

    @SuppressLint("StaticFieldLeak")
    private class RetrieveLoggedUser extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Connection connection = DBConnection.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT ID, USERNAME, PASSWORD_HASH, REGISTER_DATE " +
                        "FROM Users " +
                        "WHERE USERNAME = ?");
                preparedStatement.setString(1, PreferenceData.getUserName(activity));
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    Constants.USER.addUser(new User(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), 0, resultSet.getTimestamp(4)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class RetrieveLoggedUserOrders extends AsyncTask<Boolean, Void, Void> {

        boolean bool = false;

        @Override
        protected Void doInBackground(Boolean... params) {
            bool = params[0];
            android.os.Process.setThreadPriority(THREAD_PRIORITY_BACKGROUND + THREAD_PRIORITY_MORE_FAVORABLE);
            try {
                Connection connection = DBConnection.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT ID, DATE_CREATED, DATE_SHIPPED, CUSTOMER_NAME, USER_ID, TOTAL, STATUS, SHIPPING_ID " +
                        "FROM Orders " +
                        "WHERE USER_ID = ? " +
                        "ORDER BY DATE_CREATED DESC");
                preparedStatement.setInt(1, Constants.USER.getAllUsers().get(0).getId());
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    Constants.ORDER.addOrder(new Order(resultSet.getInt(1), resultSet.getTimestamp(2), resultSet.getTimestamp(3), resultSet.getString(4), resultSet.getInt(5),
                            resultSet.getDouble(6), resultSet.getString(7), resultSet.getInt(8)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (bool) {
                retrieveOrderDetails();
                Constants.TRANSACTION.addTransaction(new Transaction(Constants.PAYPAL_TRANSATION_ID, Constants.ORDER.getAllOrders().get(0).getId(), Constants.USER.getActualUser().getId(), "PAGATO"));
                updateTransactions(Constants.ORDER.getAllOrders().get(0).getId(), 0);

                Constants.SHOPPING_CART.getAllShoppingCarts().clear();
                Constants.CART_ITEM.getAllCartItems().clear();

                deleteShoppingCart();
                sendEmail(Constants.USER.getActualUser().getUsername(), "", "2");
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class RetrieveLastOrder extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            android.os.Process.setThreadPriority(THREAD_PRIORITY_BACKGROUND + THREAD_PRIORITY_MORE_FAVORABLE);
            try {
                Connection connection = DBConnection.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT ID, DATE_CREATED, DATE_SHIPPED, CUSTOMER_NAME, USER_ID, TOTAL, STATUS, SHIPPING_ID " +
                        "FROM Orders " +
                        "WHERE USER_ID = ? " +
                        "ORDER BY DATE_CREATED DESC " +
                        "LIMIT 1");
                preparedStatement.setInt(1, Constants.USER.getAllUsers().get(0).getId());
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    Constants.ORDER.getAllOrders().add(0, new Order(resultSet.getInt(1), resultSet.getTimestamp(2), resultSet.getTimestamp(3), resultSet.getString(4), resultSet.getInt(5),
                            resultSet.getDouble(6), resultSet.getString(7), resultSet.getInt(8)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ((Runnable) () -> {
                android.os.SystemClock.sleep(100);
                for (CartItem cartItem : Constants.CART_ITEM.getAllCartItems()) {
                    Product product = Constants.PRODUCT.getProduct(cartItem.getProductId());
                    insertOrderDetails(Constants.ORDER.getAllOrders().get(0).getId(), product.getId(), product.getName(), cartItem.getQuantity(), product.getPrice());
                }
            }).run();
        }
    }


    @SuppressLint("StaticFieldLeak")
    private class RetrieveLoggedUserData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Connection connection = DBConnection.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT EMAIL, NAME, SURNAME, DATEOFBIRTH, SEX, REGION, ADDRESS, CITY, CAP " +
                        "FROM Customers " +
                        "WHERE EMAIL = ?");
                preparedStatement.setString(1, Constants.USER.getAllUsers().get(0).getUsername());
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    Constants.CUSTOMER.addCustomer(new Customer(resultSet.getString(1), resultSet.getString(2), resultSet.getString(3), resultSet.getDate(4), resultSet.getString(5),
                            resultSet.getString(6), resultSet.getString(7), resultSet.getString(8), resultSet.getInt(9)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class RetrieveShippingData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Connection connection = DBConnection.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT ID, TYPE, COST, REGION_NAME " +
                        "FROM Shipping_Info");
                while (resultSet.next()) {
                    Constants.SHIPPING.addShippingInfo(new ShippingInfo(resultSet.getInt(1), resultSet.getString(2), resultSet.getDouble(3), resultSet.getString(4)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class RetrieveShoppingCart extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Connection connection = DBConnection.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT ID, USER_ID, PRODUCT_ID, QUANTITY " +
                        "FROM Shopping_Cart " +
                        "WHERE USER_ID = ?");
                preparedStatement.setInt(1, Constants.USER.getAllUsers().get(0).getId());
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    Constants.SHOPPING_CART.addShoppingCart(new ShoppingCart(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getInt(4)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class RetrieveProducts extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            android.os.Process.setThreadPriority(THREAD_PRIORITY_BACKGROUND + THREAD_PRIORITY_MORE_FAVORABLE);
            try {
                Connection connection = DBConnection.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT ID, NAME, DESCRIPTION, CATEGORY, PRICE, QUANTITY, IMAGE_FILENAME " +
                        "FROM Products");
                while (resultSet.next()) {
                    Constants.PRODUCT.addProduct(new Product(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4),
                            resultSet.getDouble(5), resultSet.getInt(6), resultSet.getBlob(7)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class RetrieveCategories extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Connection connection = DBConnection.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT ID, CATEGORY " +
                        "FROM Product_Categories");
                while (resultSet.next()) {
                    Constants.CATEGORY.addCategory(new ProductCategory(resultSet.getInt(1), resultSet.getString(2)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class RetrieveOrderDetails extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Connection connection = DBConnection.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT o.ID, od.PRODUCT_ID, od.PRODUCT_NAME, od.QUANTITY, od.UNIT_COST, od.SUBTOTAL " +
                        "FROM Order_Details od " +
                        "JOIN Orders o ON od.ORDER_ID = o.ID " +
                        "WHERE o.USER_ID = ?");
                preparedStatement.setInt(1, Constants.USER.getAllUsers().get(0).getId());
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    Constants.ORDER_DETAIL.addOrder(new OrderDetail(resultSet.getInt(1), resultSet.getInt(2), resultSet.getString(3),
                            resultSet.getInt(4), resultSet.getDouble(5)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class RetrieveTransactions extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Connection connection = DBConnection.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT TRANSACTION_ID, ORDER_ID, USER_ID, STATUS " +
                        "FROM Paypal_Transactions WHERE USER_ID = ?");
                preparedStatement.setInt(1, Constants.USER.getAllUsers().get(0).getId());
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    Constants.TRANSACTION.addTransaction(new Transaction(resultSet.getString(1), resultSet.getInt(2), resultSet.getInt(3),
                            resultSet.getString(4)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class DeleteShoppingCartItem extends AsyncTask<Integer, Void, Void> {

        @Override
        protected Void doInBackground(Integer... params) {
            try {
                Connection connection = DBConnection.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM Shopping_Cart " +
                        "WHERE USER_ID = ? AND PRODUCT_ID = ?");
                preparedStatement.setInt(1, Constants.USER.getAllUsers().get(0).getId());
                preparedStatement.setInt(2, params[0]);
                preparedStatement.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class DeleteShoppingCart extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Connection connection = DBConnection.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM Shopping_Cart " +
                        "WHERE USER_ID = ?");
                preparedStatement.setInt(1, Constants.USER.getAllUsers().get(0).getId());
                preparedStatement.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class updateShoppingCart extends AsyncTask<Integer, Void, Boolean> {

        boolean showMessage = false;

        @Override
        protected Boolean doInBackground(Integer... params) {
            int id = 0;
            showMessage = params[4] > 0;
            try {
                Connection connection = DBConnection.getConnection();
                PreparedStatement preparedStatement;
                if (params[3] == 0) {
                    preparedStatement = connection.prepareStatement("UPDATE Shopping_Cart " +
                            "SET QUANTITY = ? " +
                            "WHERE USER_ID = ? AND PRODUCT_ID = ?");
                    preparedStatement.setInt(1, params[1]);
                    preparedStatement.setInt(2, params[2]);
                    preparedStatement.setInt(3, params[0]);
                } else {
                    preparedStatement = connection.prepareStatement("INSERT INTO Shopping_Cart(USER_ID, PRODUCT_ID, QUANTITY) VALUES(?, ?, ?) " +
                            "ON DUPLICATE KEY UPDATE QUANTITY = QUANTITY + ?");
                    preparedStatement.setInt(1, params[2]);
                    preparedStatement.setInt(2, params[0]);
                    preparedStatement.setInt(3, params[1]);
                    preparedStatement.setInt(4, params[1]);
                }
                id = preparedStatement.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return id > 0;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (showMessage)
                if (aBoolean)
                    Toast.makeText(activity, "Prodotto aggiunto al carrello", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(activity, "Non è stato possibile aggiungere il prodotto al carrello\nRiprovare più tardi", Toast.LENGTH_LONG).show();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class InsertOrder extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            android.os.Process.setThreadPriority(THREAD_PRIORITY_BACKGROUND + THREAD_PRIORITY_MORE_FAVORABLE);
            int id = 0;
            try {
                Connection connection = DBConnection.getConnection();
                PreparedStatement preparedStatement;
                preparedStatement = connection.prepareStatement("INSERT INTO Orders(CUSTOMER_NAME, USER_ID, SHIPPING_ID) " +
                        "VALUES(?, ?, ?) ");
                preparedStatement.setString(1, params[0] + " " + params[1]);
                preparedStatement.setInt(2, Integer.valueOf(params[2]));
                preparedStatement.setInt(3, Integer.valueOf(params[3]));
                id = preparedStatement.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return id > 0;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean) {
                Toast.makeText(activity, "Ordine effettuato", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(activity, "Non è stato possibile effettuare l'ordine\nRiprovare più tardi", Toast.LENGTH_LONG).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class InsertOrderDetails extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            android.os.Process.setThreadPriority(THREAD_PRIORITY_BACKGROUND + THREAD_PRIORITY_MORE_FAVORABLE);
            if (Integer.valueOf(params[3]) > 0)
                try {
                    Connection connection = DBConnection.getConnection();
                    PreparedStatement preparedStatement;
                    preparedStatement = connection.prepareStatement("INSERT INTO Order_Details(ORDER_ID, PRODUCT_ID, PRODUCT_NAME, QUANTITY, UNIT_COST) " +
                            "VALUES(?, ?, ?, ?, ?)");
                    preparedStatement.setInt(1, Integer.valueOf(params[0]));
                    preparedStatement.setInt(2, Integer.valueOf(params[1]));
                    preparedStatement.setString(3, params[2]);
                    preparedStatement.setInt(4, Integer.valueOf(params[3]));
                    preparedStatement.setDouble(5, Double.valueOf(params[4]));
                    preparedStatement.executeUpdate();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ((Runnable) () -> {
                SystemClock.sleep(500);
                //ORDINE AGGIORNATO
                Constants.ORDER.getAllOrders().clear();
                new android.os.Handler().postDelayed(() -> {
                    retrieveLoggedUserOrders();
                }, 1000);
            }).run();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class updateTransactions extends AsyncTask<Integer, Void, Boolean> {

        boolean bool = false;

        @Override
        protected Boolean doInBackground(Integer... params) {
            int id = 0;
            bool = params[1] > 0;
            try {
                Connection connection = DBConnection.getConnection();
                PreparedStatement preparedStatement;
                preparedStatement = connection.prepareStatement("INSERT INTO Paypal_Transactions(TRANSACTION_ID, ORDER_ID, USER_ID) VALUES(?, ?, ?) " +
                        "ON DUPLICATE KEY UPDATE STATUS = 'RIMBORSATO'");
                preparedStatement.setString(1, Constants.TRANSACTION.getTransactionFromOrderID(params[0]).getTransactionID());
                preparedStatement.setInt(2, params[0]);
                preparedStatement.setInt(3, Constants.USER.getAllUsers().get(0).getId());
                id = preparedStatement.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return id > 0;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (bool)
                if (aBoolean) {
                    Toast.makeText(activity, "Rimborso effettuato", Toast.LENGTH_SHORT).show();
                    // SendHTMLEmail.sendEmail(actualUser.getUsername(), "", "3");
                } else
                    Toast.makeText(activity, "Non è stato possibile effettuare il rimborso\nRiprovare più tardi", Toast.LENGTH_LONG).show();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class CancelOrder extends AsyncTask<Integer, Void, Void> {

        @Override
        protected Void doInBackground(Integer... params) {
            try {
                Connection connection = DBConnection.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("UPDATE Orders " +
                        "SET STATUS = ? WHERE USER_ID = ? AND ID = ?");
                preparedStatement.setString(1, "RIMBORSATO");
                preparedStatement.setInt(2, Constants.USER.getAllUsers().get(0).getId());
                preparedStatement.setInt(3, params[0]);
                preparedStatement.executeQuery();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class SendEmail extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            SendHTMLEmail.sendEmail(params[0], params[1], params[2]);
            return null;
        }
    }
}
