package com.example.ingsw.ec18_04_m_v001.DAOImpl;

import com.example.ingsw.ec18_04_m_v001.DAO.ProductDAO;
import com.example.ingsw.ec18_04_m_v001.DBClasses.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductDaoImpl implements ProductDAO {

    private List<Product> products = new ArrayList<>();

    @Override
    public List<Product> getAllProducts() {
        return products;
    }

    public Product getProduct(int id) {
        Product res = null;
        for (Product p : products)
            if (p.getId() == id)
                res = p;
        return res;
    }

    @Override
    public void updateProduct(Product product) {
//        ignore
    }

    @Override
    public void deleteProduct(Product product) {
//        ignore
    }

    @Override
    public void addProduct(Product product) {
        for (Product p : products)
            if (p.equals(product))
                return;

        products.add(product);
    }
}
