package com.example.ingsw.ec18_04_m_v001.DAO;

import com.example.ingsw.ec18_04_m_v001.DBClasses.Customer;

import java.util.List;

public interface CustomerDAO {
    public List<Customer> getAllCustomers();

    public void updateCustomer(Customer customer);

    public void deleteCustomer(Customer customer);

    public void addCustomer(Customer customer);
}
