package com.example.ingsw.ec18_04_m_v001.DAO;

import com.example.ingsw.ec18_04_m_v001.DBClasses.User;

import java.util.List;

public interface UserDAO {
    public List<User> getAllUsers();

    public void updateUser(User user);

    public void deleteUser(User user);

    public void addUser(User user);
}
