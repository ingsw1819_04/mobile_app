package com.example.ingsw.ec18_04_m_v001.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ingsw.ec18_04_m_v001.DBClasses.CartItem;
import com.example.ingsw.ec18_04_m_v001.DBClasses.Customer;
import com.example.ingsw.ec18_04_m_v001.DBClasses.Product;
import com.example.ingsw.ec18_04_m_v001.DBClasses.ShoppingCart;
import com.example.ingsw.ec18_04_m_v001.DBClasses.User;
import com.example.ingsw.ec18_04_m_v001.R;
import com.example.ingsw.ec18_04_m_v001.RecyclerView.Adapter.CartAdapter;
import com.example.ingsw.ec18_04_m_v001.Utility.CRUDClass;
import com.example.ingsw.ec18_04_m_v001.Utility.Constants;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CartActivity extends AppCompatActivity implements CartAdapter.CartViewHolder.OnNoteListner {

    @SuppressLint("StaticFieldLeak")
    private static TextView shippingTotal;
    @SuppressLint("StaticFieldLeak")
    private static TextView total;
    @SuppressLint("StaticFieldLeak")
    private static TextView shippingLabel;
    @SuppressLint("StaticFieldLeak")
    private static Button buy;
    private static double shipping = 0.0;
    private static PayPalConfiguration configuration = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(Constants.PAYPAL_CLIENT_ID)
            .languageOrLocale("it");
    private List<CartItem> cartList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerAdapter;
    private RecyclerView.LayoutManager recyclerLayoutManager;
    private ProgressBar loading;
    private User actualUser = Constants.USER.getActualUser();
    private int REQUEST_CODE = 7171;

    /**
     * Calculate the total price of the items inside the cart
     */
    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    public static void calculateTotal() {
        String aux = "" + shipping;
        double ship = Double.valueOf(aux);
        double totalPrice = 0.0;
        for (CartItem c : Constants.CART_ITEM.getAllCartItems()) {
            totalPrice += c.getQuantity() * c.getUnitCost();
        }
        if (totalPrice > 0) {
            totalPrice += ship;
        }
        total.setText("" + String.format("%.2f", totalPrice).replace(",", "."));
    }

    @Override
    public void onBackPressed() {
        CRUDClass crudClass = new CRUDClass(this);
        Iterator<CartItem> iterator = Constants.CART_ITEM.getAllCartItems().iterator();
        while (iterator.hasNext()) {
            CartItem cartItem = iterator.next();
            Constants.SHOPPING_CART.getShoppingcartFromProduct(cartItem.getProductId()).setQuantity(cartItem.getQuantity());
            crudClass.updateShoppingCart(cartItem.getProductId(), cartItem.getQuantity(), actualUser.getId(), 0, false);
            if (cartItem.getQuantity() == 0) {
                iterator.remove();
                Constants.SHOPPING_CART.deleteShoppingCart(Constants.SHOPPING_CART.getShoppingcartFromProduct(cartItem.getProductId()));
                crudClass.deleteShoppingCartItem(cartItem.getProductId());
            }
        }
        finish();
    }

    @Override
    protected void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getCartItemFromShoppingCart();
        setContentView(R.layout.activity_cart);

        total = findViewById(R.id.total);
        shippingTotal = findViewById(R.id.shipping_total);
        shippingLabel = findViewById(R.id.shipping_label);
        buy = findViewById(R.id.buy_button);
        loading = findViewById(R.id.progressBar);
        loading.setVisibility(View.VISIBLE);
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerLayoutManager = new LinearLayoutManager(CartActivity.this);

        recyclerAdapter = new CartAdapter(cartList, this);

        recyclerView.setLayoutManager(recyclerLayoutManager);
        recyclerView.setAdapter(recyclerAdapter);
        loading.setVisibility(View.GONE);
        int userId = actualUser.getId();
        if (userId > 0) {
            shippingLabel.setVisibility(View.VISIBLE);
            shippingTotal.setVisibility(View.VISIBLE);
            shipping = Constants.SHIPPING.getShippinginfo(Constants.CUSTOMER.getCustomer(actualUser.getUsername()).getRegion()).getCost();
            shippingTotal.setText("€ " + String.format("%.2f", shipping).replace(",", "."));
        } else {
            shippingLabel.setVisibility(View.INVISIBLE);
            shippingTotal.setVisibility(View.INVISIBLE);
            shipping = 0.0;
            shippingTotal.setText("€" + String.format("%.2f", shipping).replace(",", "."));
        }

        // START PAYPAL SERVICE
        Intent intentPayPalService = new Intent(this, PayPalService.class);
        intentPayPalService.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, configuration);
        startService(intentPayPalService);

        calculateTotal();

        buy.setVisibility(cartList.size() > 0 ? View.VISIBLE : View.INVISIBLE);
        buy.setOnClickListener(v -> {
            if (cartList.size() > 0) {
                if (actualUser.getId() > 0) {
                    processPayment();
                } else {
                    Intent intent = new Intent(this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    /**
     * Initialize the order payment process
     */
    private void processPayment() {
        double cost = Double.valueOf(total.getText().toString());
        PayPalPayment payPalPayment = new PayPalPayment(new BigDecimal(cost), "EUR", "Ordine di " + actualUser.getUsername(), PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intentPaymentActivity = new Intent(this, PaymentActivity.class);
        intentPaymentActivity.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, configuration);
        intentPaymentActivity.putExtra(PaymentActivity.EXTRA_PAYMENT, payPalPayment);
        startActivityForResult(intentPaymentActivity, REQUEST_CODE);
    }

    /**
     * @param position
     */
    public void onNoteClick(int position) {
        // ignore
    }

    /**
     * The method allows you to retrieve all the items inside the cart
     */
    private void getCartItemFromShoppingCart() {
        List<ShoppingCart> subList = new ArrayList<>();
        for (ShoppingCart sc : Constants.SHOPPING_CART.getAllShoppingCarts())
            if (sc.getUserId() == actualUser.getId()) {
                subList.add(sc);
            }

        for (ShoppingCart sc : subList) {
            Product product = Constants.PRODUCT.getProduct(sc.getProductId());
            Constants.CART_ITEM.addCartItem(new CartItem(product.getId(), product.getName(), sc.getQuantity(), product.getPrice(), product.getImage()));
        }
        cartList = Constants.CART_ITEM.getAllCartItems();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String price = total.getText().toString();
                PaymentConfirmation paymentConfirmation = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (paymentConfirmation != null) {
                    Customer actualCustomer = Constants.CUSTOMER.getCustomer(actualUser.getUsername());
                    try {
                        String paymentDetails = paymentConfirmation.toJSONObject().toString(4);
                        startActivity(new Intent(this, PaymentDetails.class)
                                .putExtra("PaymentDetails", paymentDetails)
                                .putExtra("PaymentTotal", price));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    CRUDClass crudClass = new CRUDClass(this);
                    crudClass.insertOrder(actualCustomer.getName(), actualCustomer.getSurname(), actualUser.getId(), Constants.SHIPPING.getShippinginfo(actualCustomer.getRegion()).getId());
                    new android.os.Handler().postDelayed(() -> {
                        Constants.ORDER.getAllOrders().clear();
                        Constants.ORDER_DETAIL.getAllOrderDetails().clear();
                        crudClass.retrieveLastOrder();
                        finish();
                    }, 300);
                }
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Pagamento cancellato", Toast.LENGTH_SHORT).show();
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Toast.makeText(this, "Pagamento non valido", Toast.LENGTH_LONG).show();
            }
        }
    }
}
