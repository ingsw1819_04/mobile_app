package com.example.ingsw.ec18_04_m_v001.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.ingsw.ec18_04_m_v001.DBClasses.Order;
import com.example.ingsw.ec18_04_m_v001.R;
import com.example.ingsw.ec18_04_m_v001.RecyclerView.Adapter.OrderAdapter;
import com.example.ingsw.ec18_04_m_v001.RecyclerView.OrderItem;
import com.example.ingsw.ec18_04_m_v001.Utility.CRUDClass;
import com.example.ingsw.ec18_04_m_v001.Utility.Constants;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class OrderActivity extends AppCompatActivity implements OrderAdapter.OrderViewHolder.OnNoteListner {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerAdapter;
    private RecyclerView.LayoutManager recyclerLayoutManager;
    private ArrayList<OrderItem> orderList = new ArrayList<>();
    private boolean restored = false;
    private SwipeRefreshLayout refreshLayout;

    /**
     * "Draw" the background when an order is deleted
     *
     * @param context Activity where "drawing"
     * @return Result of the "drawing"
     */
    private static Bitmap getBitmapFromVectorDrawable(Context context) {
        Drawable drawable = ContextCompat.getDrawable(context, R.drawable.ic_delete);

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        convertOrderToOrderItem();

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerLayoutManager = new LinearLayoutManager(this);

        recyclerAdapter = new OrderAdapter(orderList, this);
        recyclerView.setLayoutManager(recyclerLayoutManager);
        recyclerView.setAdapter(recyclerAdapter);

        CRUDClass crudClass = new CRUDClass(this);
        crudClass.retrieveOrderDetails();

        refreshLayout = findViewById(R.id.refreshLayout);
        refreshLayout.setColorSchemeResources(R.color.colorPrimaryDark);

        refreshLayout.setOnRefreshListener(() -> {
            orderList.clear();
            convertOrderToOrderItem();
            recyclerAdapter.notifyDataSetChanged();
            refreshLayout.setRefreshing(false);
        });

        Paint paint = new Paint();

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder target, int direction) {
                int position = target.getAdapterPosition();
                restored = false;
                OrderItem orderItem = orderList.get(position);
                if (orderItem.getStatus().equals("EFFETTUATO")) {
                    orderList.remove(position);
                    Snackbar snackbar = Snackbar.make(findViewById(R.id.constraint_layout), "Ordine rimosso", Snackbar.LENGTH_LONG);
                    snackbar.setAction("UNDO", view -> {
                        orderList.add(position, orderItem);
                        restored = true;
                        recyclerAdapter.notifyItemChanged(position);
                    });
                    snackbar.setActionTextColor(Color.YELLOW);
                    snackbar.show();
                    new android.os.Handler().postDelayed(() -> {
                        if (!restored) {
                            orderItem.setStatus("RIMBORSATO");
                            int orderID = orderItem.getId();
                            crudClass.cancelOrder(orderID);
                            crudClass.updateTransactions(orderID, 1);
                            Order order = Constants.ORDER.getOrder(orderID);
                            order.setStatus(orderItem.getStatus());
                            Constants.ORDER.updateOrder(order);
                            orderList.add(position, orderItem);
                            recyclerAdapter.notifyItemChanged(position);
                            crudClass.sendEmail(Constants.USER.getActualUser().getUsername(), "", "3");
                        }
                    }, 3000);
                } else {
                    Toast.makeText(OrderActivity.this, "Non puoi cancellare un ordine spedito, ricevuto o già rimborsato", Toast.LENGTH_LONG).show();
                }
                recyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildDraw(@NonNull Canvas canvas, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;
                    if (dX < 0) {
                        paint.setColor(Color.RED);
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        canvas.drawRect(background, paint);
                        icon = getBitmapFromVectorDrawable(OrderActivity.this);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        canvas.drawBitmap(icon, null, icon_dest, paint);
                    }
                }
                super.onChildDraw(canvas, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        });

        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    /**
     * Create a list for the orders items
     */
    private void convertOrderToOrderItem() {
        for (Order o : Constants.ORDER.getAllOrders()) {
            orderList.add(new OrderItem(o.getId(), o.getTotal(), o.getDateCreate(), o.getStatus(), Constants.SHIPPING.getShippinginfo(o.getShippingId()).getCost()));
        }
    }

    @Override
    public void onNoteClick(int position) {
        Intent intentOrderDetailActivity = new Intent(this, OrderDetailActivity.class);
        OrderItem selected = orderList.get(position);

        intentOrderDetailActivity.putExtra("ORDER_ID", selected.getId());
        startActivity(intentOrderDetailActivity);
    }
}
