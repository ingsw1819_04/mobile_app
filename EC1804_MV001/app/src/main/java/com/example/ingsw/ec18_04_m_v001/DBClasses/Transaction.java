package com.example.ingsw.ec18_04_m_v001.DBClasses;

public class Transaction {

    private String transactionID;
    private int orderID;
    private int userID;
    private String status;

    public Transaction(String transactionID, int orderID, int userID, String status) {
        this.transactionID = transactionID;
        this.orderID = orderID;
        this.userID = userID;
        this.status = status;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof Transaction))
            return false;

        Transaction t = (Transaction) obj;
        return transactionID.equals(t.transactionID);
    }
}
