package com.example.ingsw.ec18_04_m_v001.RecyclerView;

import java.sql.Timestamp;

public class OrderItem {

    private int id;
    private double total;
    private Timestamp creationDate;
    private String status;
    private double shippingCost;

    public OrderItem(int id, double total, Timestamp creationDate, String status, double shippingCost) {
        this.id = id;
        this.total = total;
        this.creationDate = creationDate;
        this.status = status;
        this.shippingCost = shippingCost;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(double shippingCost) {
        this.shippingCost = shippingCost;
    }
}
