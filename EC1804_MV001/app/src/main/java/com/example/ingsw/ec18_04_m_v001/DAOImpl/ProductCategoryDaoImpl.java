package com.example.ingsw.ec18_04_m_v001.DAOImpl;

import com.example.ingsw.ec18_04_m_v001.DAO.ProductCategoryDAO;
import com.example.ingsw.ec18_04_m_v001.DBClasses.ProductCategory;

import java.util.ArrayList;
import java.util.List;

public class ProductCategoryDaoImpl implements ProductCategoryDAO {

    private List<ProductCategory> categories = new ArrayList<>();

    @Override
    public List<ProductCategory> getAllCategories() {
        return categories;
    }

    @Override
    public void updateCategory(ProductCategory category) {
//        ignore
    }

    @Override
    public void deleteCategory(ProductCategory category) {
//        ignore
    }

    @Override
    public void addCategory(ProductCategory category) {
        for (ProductCategory pc : categories)
            if (pc.equals(category))
                return;

        categories.add(category);
    }
}
