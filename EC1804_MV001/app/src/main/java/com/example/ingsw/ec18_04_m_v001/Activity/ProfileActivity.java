package com.example.ingsw.ec18_04_m_v001.Activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ingsw.ec18_04_m_v001.Connection.DBConnection;
import com.example.ingsw.ec18_04_m_v001.DBClasses.Customer;
import com.example.ingsw.ec18_04_m_v001.Encoding.SHAEncryption;
import com.example.ingsw.ec18_04_m_v001.R;
import com.example.ingsw.ec18_04_m_v001.Utility.Constants;
import com.example.ingsw.ec18_04_m_v001.Utility.PreferenceData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static java.util.Calendar.DATE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;

public class ProfileActivity extends AppCompatActivity {

    // Customer DATA
    Customer customer;
    // UI Password
    private Switch switchPassword;
    private EditText passwordEditText;
    private EditText password2EditText;
    private Button savePasswordButton;
    // UI Shipping
    private Switch switchShipping;
    private EditText addressEditText;
    private EditText cityEditText;
    private AutoCompleteTextView regionACTV;
    private EditText capEditText;
    private Button saveShippingButton;
    // UI Profile
    private Switch switchProfile;
    private EditText nameEditText;
    private EditText surnameEditText;
    private DatePickerDialog datePickerDialog;
    private EditText birthdateEditText;
    private RadioGroup sexRadioGroup;
    private RadioButton fRadioButton;
    private RadioButton mRadioButton;
    private Button saveProfileButton;

    /**
     * Recover the relevant date in the form of a calendar
     *
     * @param date Data to be transformed
     * @return Date in calendar form
     */
    private static Calendar getCalendar(java.util.Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        customer = Constants.CUSTOMER.getCustomer(Constants.USER.getActualUser().getUsername());
        prepareUIComponents();
        setUIComponents();
    }

    /**
     * Prepare the component interface
     */
    @SuppressLint("ClickableViewAccessibility")
    private void prepareUIComponents() {
        preparePasswordComponents();
        prepareShippingComponents();
        prepareProfileComponents();
    }

    /**
     * Organize the password management interface
     */
    private void preparePasswordComponents() {
        switchPassword = findViewById(R.id.switch_password);
        passwordEditText = findViewById(R.id.password);
        password2EditText = findViewById(R.id.password2);
        savePasswordButton = findViewById(R.id.save_password_button);

        switchPassword.setOnCheckedChangeListener((buttonView, isChecked) -> {
            boolean switchPasswordChecked = switchPassword.isChecked();
            passwordEditText.setEnabled(switchPasswordChecked);
            password2EditText.setEnabled(switchPasswordChecked);
            savePasswordButton.setEnabled(switchPasswordChecked);
        });

        passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (passwordEditText.isEnabled()) {
                    if (!isPasswordValid(passwordEditText.getText().toString())) {
                        passwordEditText.setError(getString(R.string.error_invalid_password));
                    }
                }
            }
        });

        password2EditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (passwordEditText.isEnabled()) {
                    if (!passwordEditText.getText().toString().equals(password2EditText.getText().toString())) {
                        password2EditText.setError(getString(R.string.error_nomatch_password));
                    }
                }
            }
        });

        savePasswordButton.setOnClickListener(v -> {
            if (passwordEditText.getError() == null && password2EditText.getError() == null) {
                String unhashedPassword = passwordEditText.getText().toString();
                Constants.USER.getActualUser().setPasswordHash(SHAEncryption.encryptThisString(unhashedPassword));
                ChangePassword changePassword = new ChangePassword();
                changePassword.execute(unhashedPassword);
                switchPassword.setChecked(false);
                passwordEditText.setText("");
                password2EditText.setText("");
            }
        });
    }

    /**
     * Organize the shipping management interface
     */
    private void prepareShippingComponents() {
        switchShipping = findViewById(R.id.switch_shipping);
        addressEditText = findViewById(R.id.address);
        cityEditText = findViewById(R.id.city);
        capEditText = findViewById(R.id.cap);
        regionACTV = findViewById(R.id.region);
        String[] regions = getResources().getStringArray(R.array.regions_array);
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, regions);
        regionACTV.setAdapter(adapter);
        saveShippingButton = findViewById(R.id.save_shipping_button);

        switchShipping.setOnCheckedChangeListener((buttonView, isChecked) -> {
            boolean switchShippingChecked = switchShipping.isChecked();
            addressEditText.setEnabled(switchShippingChecked);
            cityEditText.setEnabled(switchShippingChecked);
            capEditText.setEnabled(switchShippingChecked);
            regionACTV.setEnabled(switchShippingChecked);
            saveShippingButton.setEnabled(switchShippingChecked);
        });

        addressEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isFieldValid(addressEditText.getText().toString())) {
                    addressEditText.setError(getString(R.string.error_field_required));
                }
            }
        });

        cityEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isFieldValid(cityEditText.getText().toString())) {
                    cityEditText.setError(getString(R.string.error_field_required));
                }
            }
        });

        capEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                String cap = capEditText.getText().toString();
                if (!isCapValid(cap)) {
                    capEditText.setError(getString(R.string.error_invalid_cap));
                } else if (cap.length() < 5) {
                    capEditText.setError(getString(R.string.error_short_cap));
                }
            }
        });

        regionACTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isRegionValid(regionACTV.getText().toString())) {
                    regionACTV.setError(getString(R.string.error_invalid_region));
                } else {
                    regionACTV.setError(null);
                }
            }
        });

        saveShippingButton.setOnClickListener(v -> {
            if (addressEditText.getError() == null && regionACTV.getError() == null && capEditText.getError() == null) {
                updateCustomerData();
                switchShipping.setChecked(false);
            }
        });
    }

    /**
     * Organize the profile management interface
     */
    @SuppressLint("ClickableViewAccessibility")
    private void prepareProfileComponents() {
        switchProfile = findViewById(R.id.switch_profile);
        nameEditText = findViewById(R.id.product_name);
        surnameEditText = findViewById(R.id.surname);
        sexRadioGroup = findViewById(R.id.sexRadioGroup);
        fRadioButton = findViewById(R.id.fRadioButton);
        mRadioButton = findViewById(R.id.mRadioButton);
        birthdateEditText = findViewById(R.id.birthdate);
        birthdateEditText.setShowSoftInputOnFocus(false);
        saveProfileButton = findViewById(R.id.save_profile_button);
        prepareDatePickerDialog();
        birthdateEditText.setOnTouchListener((v, event) -> {
            birthdateEditText.setInputType(InputType.TYPE_NULL);
            datePickerDialog.show();
            return true;
        });

        birthdateEditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                birthdateEditText.setInputType(InputType.TYPE_NULL);
                datePickerDialog.show();
            }
        });

        switchProfile.setOnCheckedChangeListener((buttonView, isChecked) -> {
            boolean switchProfileChecked = switchProfile.isChecked();
            nameEditText.setEnabled(switchProfileChecked);
            surnameEditText.setEnabled(switchProfileChecked);
            birthdateEditText.setEnabled(switchProfileChecked);
            fRadioButton.setEnabled(switchProfileChecked);
            mRadioButton.setEnabled(switchProfileChecked);
            saveProfileButton.setEnabled(switchProfileChecked);
        });

        nameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!isFieldValid(nameEditText.getText().toString())) {
                    nameEditText.setError(getString(R.string.error_field_required));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // ignore
            }
        });

        surnameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!isFieldValid(surnameEditText.getText().toString())) {
                    surnameEditText.setError(getString(R.string.error_field_required));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // ignore
            }
        });

        birthdateEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (birthdateEditText.isEnabled()) {
                    if (!isDateValid(birthdateEditText.getText().toString())) {
                        birthdateEditText.setFocusable(true);
                        birthdateEditText.setError(getString(R.string.error_age_restriction));
                        birthdateEditText.requestFocus();
                    } else {
                        birthdateEditText.setError(null);
                    }
                }
            }
        });

        saveProfileButton.setOnClickListener(v -> {
            if (nameEditText.getError() == null && surnameEditText.getError() == null && birthdateEditText.getError() == null && (fRadioButton.isChecked() || mRadioButton.isChecked())) {
                updateCustomerData();
                switchProfile.setChecked(false);
            }
        });
    }

    /**
     * Set the component interface
     */
    private void setUIComponents() {
        setPasswordComponents();
        setShippingComponents();
        setProfileComponents();
    }

    /**
     * Set the password management interface
     */
    private void setPasswordComponents() {
        passwordEditText.setText("");
        password2EditText.setText("");
    }

    /**
     * Set the shipping management interface
     */
    @SuppressLint("SetTextI18n")
    private void setShippingComponents() {
        addressEditText.setText(customer.getAddress());
        cityEditText.setText(customer.getCity());
        capEditText.setText("" + customer.getCap());
        regionACTV.setText(customer.getRegion());
    }

    /**
     * Set the password management interface
     */
    private void setProfileComponents() {
        nameEditText.setText(customer.getName());
        surnameEditText.setText(customer.getSurname());
        birthdateEditText.setText(customer.getDateofbirth().toString());
        fRadioButton.setChecked(customer.getSex().equals("F"));
        mRadioButton.setChecked(customer.getSex().equals("M"));
    }

    /**
     * Organize the datepicker management dialog
     */
    @SuppressLint("SetTextI18n")
    private void prepareDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();

        datePickerDialog = new DatePickerDialog(this, (view, year, monthOfYear, dayOfMonth) -> {
            ++monthOfYear;
            birthdateEditText.setText(year + "-" + monthOfYear + "-" + dayOfMonth);
            datePickerDialog.dismiss();
        }, calendar.get(YEAR), calendar.get(MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * Check if the entered region is a valid region
     *
     * @param region Region to be verified
     * @return Validity of the region
     */
    private boolean isRegionValid(String region) {
        return (region.equalsIgnoreCase("Abruzzo") || region.equalsIgnoreCase("Basilicata") || region.equalsIgnoreCase("Calabria")
                || region.equalsIgnoreCase("Campania") || region.equalsIgnoreCase("Emilia-Romagna") || region.equalsIgnoreCase("Friuli-Venezia Giulia")
                || region.equalsIgnoreCase("Lazio") || region.equalsIgnoreCase("Liguria") || region.equalsIgnoreCase("Lombardia")
                || region.equalsIgnoreCase("Marche") || region.equalsIgnoreCase("Molise") || region.equalsIgnoreCase("Piemonte")
                || region.equalsIgnoreCase("Puglia") || region.equalsIgnoreCase("Sardegna") || region.equalsIgnoreCase("Sicilia")
                || region.equalsIgnoreCase("Toscana") || region.equalsIgnoreCase("Trentino-Alto Adige") || region.equalsIgnoreCase("Umbria")
                || region.equalsIgnoreCase("Veneto") || region.equalsIgnoreCase("Valle D\'Aosta"));
    }

    /**
     * Check if the field is empty
     *
     * @param field Field to be verified
     * @return Validity of control
     */
    private boolean isFieldValid(String field) {
        return !field.isEmpty();
    }

    /**
     * Check if the password is valid
     *
     * @param password String to verify
     * @return Password verification
     */
    private boolean isPasswordValid(String password) {
        return password.length() > 5;
    }

    /**
     * Check if the date is a valid date
     *
     * @param date Date to be verified
     * @return Validity of the date
     */
    private boolean isDateValid(String date) {
        @SuppressLint("SimpleDateFormat")
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date datebirth;
        try {
            datebirth = format.parse(date);
        } catch (ParseException pe) {
            return false;
        }
        return (getYears(Calendar.getInstance().getTime(), datebirth) > 17);
    }

    /**
     * Check that the current year and date are different
     *
     * @param today Current date
     * @param date  Date of birth
     * @return The difference between the current date and the birth date
     */
    private int getYears(java.util.Date today, java.util.Date date) {
        Calendar calendarDate = getCalendar(date);
        Calendar calendarToday = getCalendar(today);
        int diff = calendarToday.get(YEAR) - calendarDate.get(YEAR);
        if (calendarDate.get(MONTH) > calendarToday.get(MONTH) ||
                (calendarDate.get(MONTH) == calendarToday.get(MONTH) && calendarDate.get(DATE) > calendarToday.get(DATE))) {
            diff--;
        }
        return diff;
    }

    /**
     * Check if the cap is correct
     *
     * @param cap Cap to be verified
     * @return Verification of the cap
     */
    private boolean isCapValid(String cap) {
        int value;
        try {
            value = Integer.parseInt(cap);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return value > 9 && value < 98169;
    }

    /**
     * Update customer data
     */
    private void updateCustomerData() {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String birthdate = birthdateEditText.getText().toString();
        System.out.println("++++++DEBUG: " + birthdate);
        java.util.Date date = null;
        try {
            date = simpleDateFormat.parse(birthdate);
            System.out.println("++++++DEBUG: " + date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        customer = new Customer(customer.getEmail(), nameEditText.getText().toString(), surnameEditText.getText().toString(), new java.sql.Date(date.getTime()), fRadioButton.isChecked() ? "F" : "M",
                regionACTV.getText().toString(), addressEditText.getText().toString(), cityEditText.getText().toString(), Integer.parseInt(capEditText.getText().toString()));
        Constants.CUSTOMER.updateCustomer(customer);

        UpdateCurrentCustomer updateCurrentCustomer = new UpdateCurrentCustomer();
        updateCurrentCustomer.execute(customer);
    }

    private class ChangePassword extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            int updatedQueries = Integer.MIN_VALUE;
            try {
                String query = "UPDATE Users " +
                        "SET PASSWORD_HASH = ? " +
                        "WHERE USERNAME = ?";
                Connection connection = DBConnection.getConnection();

                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, params[0]);
                preparedStatement.setString(2, PreferenceData.getUserName(ProfileActivity.this));
                updatedQueries = preparedStatement.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return updatedQueries > 0;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean) {
                Toast.makeText(ProfileActivity.this, "Password modificata", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(ProfileActivity.this, "Non è stato possibile modificare la password\nRiprovare più tardi", Toast.LENGTH_LONG).show();
            }
        }
    }

    private class UpdateCurrentCustomer extends AsyncTask<Customer, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Customer... customers) {
            Customer customer = customers[0];
            int updatedQueries = Integer.MIN_VALUE;
            try {
                String query = "UPDATE Customers " +
                        "SET NAME = ?, SURNAME = ?, DATEOFBIRTH = ?, SEX = ?, REGION = ?, ADDRESS = ?, CITY = ?, CAP = ? " +
                        "WHERE EMAIL = ?";
                Connection connection = DBConnection.getConnection();

                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, customer.getName());
                preparedStatement.setString(2, customer.getSurname());
                preparedStatement.setDate(3, customer.getDateofbirth());
                preparedStatement.setString(4, customer.getSex());
                preparedStatement.setString(5, customer.getRegion());
                preparedStatement.setString(6, customer.getAddress());
                preparedStatement.setString(7, customer.getCity());
                preparedStatement.setInt(8, customer.getCap());
                preparedStatement.setString(9, Constants.USER.getActualUser().getUsername());
                updatedQueries = preparedStatement.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return updatedQueries > 0;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean) {
                Toast.makeText(ProfileActivity.this, "Dati modificati", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(ProfileActivity.this, "Non è stato possibile modificare i dati\nRiprovare più tardi", Toast.LENGTH_LONG).show();
            }
        }
    }
}
