package com.example.ingsw.ec18_04_m_v001.Connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.example.ingsw.ec18_04_m_v001.Utility.Constants.PASSWORD;
import static com.example.ingsw.ec18_04_m_v001.Utility.Constants.URL;
import static com.example.ingsw.ec18_04_m_v001.Utility.Constants.USERNAME;

public class DBConnection {

    private static DBConnection instance;
    private static Connection connection = null;

    private DBConnection() {
    }

    /**
     * This method allows establishing a connection with the database
     *
     * @return connection status
     */
    public static Connection getConnection() {
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    /**
     * @return the connection instance
     * @throws SQLException
     */
    public static DBConnection getInstance() throws SQLException {
        if (instance == null) {
            instance = new DBConnection();
        } else if (instance.getConnection().isClosed()) {
            instance = new DBConnection();
        }
        return instance;
    }
}