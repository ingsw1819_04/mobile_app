package com.example.ingsw.ec18_04_m_v001.DAOImpl;

import com.example.ingsw.ec18_04_m_v001.DAO.OrderDAO;
import com.example.ingsw.ec18_04_m_v001.DBClasses.Order;

import java.util.ArrayList;
import java.util.List;

public class OrderDaoImpl implements OrderDAO {

    private List<Order> orders = new ArrayList<>();

    @Override
    public List<Order> getAllOrders() {
        return orders;
    }

    public Order getOrder(int id) {
        Order res = null;
        for (Order o : orders)
            if (o.getId() == id)
                res = o;
        return res;
    }

    @Override
    public void updateOrder(Order order) {
        Order o = getOrder(order.getId());
        o.setDateCreate(order.getDateCreate());
        o.setDateShipped(order.getDateShipped());
        o.setCustomerName(order.getCustomerName());
        o.setTotal(order.getTotal());
        o.setStatus(order.getStatus());
        o.setShippingId(order.getShippingId());
    }

    @Override
    public void deleteOrder(Order order) {
//        ignore
    }

    @Override
    public void addOrder(Order order) {
        for (Order o : orders)
            if (o.equals(order))
                updateOrder(order);

        orders.add(order);
    }
}
