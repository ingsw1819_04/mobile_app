package com.example.ingsw.ec18_04_m_v001.RecyclerView;

import java.sql.Blob;

public class ProductItem {

    private int id;
    private String name;
    private double price;
    private Blob image;

    public ProductItem(int id, String name, double price, Blob image) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Blob getImage() {
        return image;
    }

    public void setImage(Blob image) {
        this.image = image;
    }
}