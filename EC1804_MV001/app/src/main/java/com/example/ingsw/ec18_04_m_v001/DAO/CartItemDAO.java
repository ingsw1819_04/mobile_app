package com.example.ingsw.ec18_04_m_v001.DAO;

import com.example.ingsw.ec18_04_m_v001.DBClasses.CartItem;

import java.util.List;

public interface CartItemDAO {
    public List<CartItem> getAllCartItems();

    public void updateCartItem(CartItem cartItem);

    public void deleteCartItem(CartItem cartItem);

    public void addCartItem(CartItem cartItem);
}
