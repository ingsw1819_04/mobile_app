package com.example.ingsw.ec18_04_m_v001.RecyclerView.Adapter;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ingsw.ec18_04_m_v001.R;
import com.example.ingsw.ec18_04_m_v001.RecyclerView.ProductItem;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter {

    private List<ProductItem> productList;
    private ProductViewHolder.OnNoteListner onNoteListner;

    public ProductAdapter(List<ProductItem> productList, ProductViewHolder.OnNoteListner onNoteListner) {
        this.productList = productList;
        this.onNoteListner = onNoteListner;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item, parent, false);
        ProductViewHolder ovh = new ProductViewHolder(v, onNoteListner);
        return ovh;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ProductItem currentItem = productList.get(i);

        ProductViewHolder ovh = (ProductViewHolder) viewHolder;
        ovh.price.setText(" € " + currentItem.getPrice());
        ovh.name.setText(currentItem.getName());

        Blob b = currentItem.getImage();
        byte[] byteArray = null;
        try {
            byteArray = b.getBytes(1, (int) (b.length()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        ovh.productImage.setImageBitmap(bitmap);
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public static class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView productImage;
        private TextView name;
        private TextView price;
        private OnNoteListner onNoteListner;

        private ProductViewHolder(@NonNull View itemView, OnNoteListner onNoteListner) {
            super(itemView);
            productImage = itemView.findViewById(R.id.product_image);
            name = itemView.findViewById(R.id.product_name);
            price = itemView.findViewById(R.id.product_price);
            this.onNoteListner = onNoteListner;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onNoteListner.onNoteClick(getAdapterPosition());
        }

        public interface OnNoteListner {
            void onNoteClick(int position);
        }
    }
}
