package com.example.ingsw.ec18_04_m_v001.DBClasses;

public class ProductCategory {

    private int id;
    private String category;

    public ProductCategory(int id, String category) {
        this.id = id;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof ProductCategory))
            return false;

        ProductCategory pc = (ProductCategory) obj;
        return id == pc.id;
    }
}
