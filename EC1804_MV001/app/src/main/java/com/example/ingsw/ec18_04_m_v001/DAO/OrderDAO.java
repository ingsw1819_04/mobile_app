package com.example.ingsw.ec18_04_m_v001.DAO;

import com.example.ingsw.ec18_04_m_v001.DBClasses.Order;

import java.util.List;

public interface OrderDAO {
    public List<Order> getAllOrders();

    public void updateOrder(Order customer);

    public void deleteOrder(Order customer);

    public void addOrder(Order customer);
}
