package com.example.ingsw.ec18_04_m_v001.DAO;

import com.example.ingsw.ec18_04_m_v001.DBClasses.Product;

import java.util.List;

public interface ProductDAO {
    public List<Product> getAllProducts();

    public void updateProduct(Product product);

    public void deleteProduct(Product product);

    public void addProduct(Product product);
}
