package com.example.ingsw.ec18_04_m_v001.RecyclerView.Adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ingsw.ec18_04_m_v001.R;
import com.example.ingsw.ec18_04_m_v001.RecyclerView.OrderItem;

import java.util.ArrayList;

public class OrderAdapter extends RecyclerView.Adapter {

    private ArrayList<OrderItem> orderList;
    private OrderViewHolder.OnNoteListner onNoteListner;

    public OrderAdapter(ArrayList<OrderItem> orderList, OrderViewHolder.OnNoteListner onNoteListner) {
        this.orderList = orderList;
        this.onNoteListner = onNoteListner;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_item, parent, false);
        OrderViewHolder ovh = new OrderViewHolder(v, onNoteListner);
        return ovh;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        OrderItem currentItem = orderList.get(i);

        OrderViewHolder ovh = (OrderViewHolder) viewHolder;
        ovh.total.setText("€ " + currentItem.getTotal());
        ovh.createDate.setText("" + currentItem.getCreationDate());
        ovh.status.setText(currentItem.getStatus());
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public static class OrderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView total;
        public TextView createDate;
        public TextView status;
        public OnNoteListner onNoteListner;

        public OrderViewHolder(@NonNull View itemView, OnNoteListner onNoteListner) {
            super(itemView);
            total = itemView.findViewById(R.id.total);
            createDate = itemView.findViewById(R.id.createDate);
            status = itemView.findViewById(R.id.status);
            this.onNoteListner = onNoteListner;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onNoteListner.onNoteClick(getAdapterPosition());
        }

        public interface OnNoteListner {
            void onNoteClick(int position);
        }
    }
}
