package com.example.ingsw.ec18_04_m_v001.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ingsw.ec18_04_m_v001.Connection.DBConnection;
import com.example.ingsw.ec18_04_m_v001.DBClasses.Customer;
import com.example.ingsw.ec18_04_m_v001.DBClasses.User;
import com.example.ingsw.ec18_04_m_v001.R;
import com.example.ingsw.ec18_04_m_v001.Utility.SendHTMLEmail;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConfirmRegistrationActivity extends AppCompatActivity {

    // UI references
    private TextView birthdateTextView;
    private Button registerButton;
    private TextView usernameTextView;
    private TextView passwordTextView;
    private TextView nameTextView;
    private TextView addressTextView;
    private TextView region;
    private ProgressBar loading;
    private boolean addedCustomer = false;
    private Bundle extras;

    @SuppressLint({"ClickableViewAccessibility", "SetTextI18n"})
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_registration);

        extras = getIntent().getExtras();

        usernameTextView = findViewById(R.id.username);
        passwordTextView = findViewById(R.id.password);
        nameTextView = findViewById(R.id.product_name);
        addressTextView = findViewById(R.id.address);
        registerButton = findViewById(R.id.register);
        birthdateTextView = findViewById(R.id.birthdate);
        region = findViewById(R.id.region);
        loading = findViewById(R.id.progressBar);

        usernameTextView.setText(extras.getString("EMAIL"));
        passwordTextView.setText(extras.getString("PASSWORD"));
        addressTextView.setText(extras.getString("ADDRESS") + ", " + extras.getString("CAP"));
        birthdateTextView.setText(extras.getString("BIRTHDATE"));
        nameTextView.setText(extras.getString("NAME") + " " + extras.getString("SURNAME") + " (" + extras.getString("SEX") + ")");
        region.setText(extras.getString("CITY") + ", " + extras.getString("REGION"));

        registerButton.setOnClickListener(v -> {
            createNewUser();
            new android.os.Handler().postDelayed(() -> {
                createNewCustomer();
                setResult(Activity.RESULT_OK);
            }, 500);
        });
    }

    /**
     * Allows the registration of a new user
     */
    private void createNewUser() {
        User user = new User(666, usernameTextView.getText().toString(), passwordTextView.getText().toString(), 0, null);
        UserRegistration userRegistration = new UserRegistration();
        userRegistration.execute(user.getUsername(), user.getPasswordHash());
    }

    /**
     * Allows the registration of a new customer
     */
    private void createNewCustomer() {
        Date date = null;

        @SuppressLint("SimpleDateFormat")
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = dateFormat.parse(birthdateTextView.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Customer customer = new Customer(usernameTextView.getText().toString(), extras.getString("NAME"), extras.getString("SURNAME"),
                new java.sql.Date(date.getTime()), extras.getString("SEX"), extras.getString("REGION"), extras.getString("ADDRESS"),
                extras.getString("CITY"), Integer.parseInt(extras.getString("CAP")));
        CustomerRegistration customerRegistration = new CustomerRegistration();
        customerRegistration.execute(customer);
    }

    @SuppressLint("StaticFieldLeak")
    private class UserRegistration extends AsyncTask<String, Void, Boolean> {

        String username = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(String... params) {
            int id = 0;
            username = params[0];
            try {
                String query = "INSERT INTO Users(USERNAME, PASSWORD_HASH) VALUES(?, ?)";
                Connection connection = DBConnection.getConnection();

                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, username);
                preparedStatement.setString(2, params[1]);
                id = preparedStatement.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return id > 0;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            addedCustomer = success;
            if (success) {
                SendMailTask sendMailTask = new SendMailTask();
                sendMailTask.execute(username, "", "0");
            } else {
                Toast.makeText(ConfirmRegistrationActivity.this, "Utente non registrato", Toast.LENGTH_LONG).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class SendMailTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            SendHTMLEmail.sendEmail(strings[0], strings[1], strings[2]);
            return "OK";
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class CustomerRegistration extends AsyncTask<Customer, Void, Boolean> {

        String username = null;

        @Override
        protected Boolean doInBackground(Customer... params) {
            int id = 0;
            try {
                String query = "INSERT INTO Customers VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
                Connection connection = DBConnection.getConnection();

                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, params[0].getEmail());
                preparedStatement.setString(2, params[0].getName());
                preparedStatement.setString(3, params[0].getSurname());
                preparedStatement.setDate(4, params[0].getDateofbirth());
                preparedStatement.setString(5, params[0].getSex());
                preparedStatement.setString(6, params[0].getRegion());
                preparedStatement.setString(7, params[0].getAddress());
                preparedStatement.setString(8, params[0].getCity());
                preparedStatement.setInt(9, params[0].getCap());
                id = preparedStatement.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return id > 0;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            loading.setVisibility(View.GONE);
            if (success && addedCustomer) {
                Toast.makeText(ConfirmRegistrationActivity.this, "Utente registrato", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toast.makeText(ConfirmRegistrationActivity.this, "Errore dati profilo\nLa invitiamo a loggarsi e modificare i dati del profilo", Toast.LENGTH_LONG).show();
            }
        }
    }
}
