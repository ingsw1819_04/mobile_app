package com.example.ingsw.ec18_04_m_v001.DAOImpl;

import com.example.ingsw.ec18_04_m_v001.DAO.TransactionDAO;
import com.example.ingsw.ec18_04_m_v001.DBClasses.Transaction;

import java.util.ArrayList;
import java.util.List;

public class TransactionDaoImpl implements TransactionDAO {

    private List<Transaction> transactions = new ArrayList<>();

    @Override
    public List<Transaction> getAllTransactions() {
        return transactions;
    }

    public Transaction getTransactionFromID(String transactionID) {
        Transaction res = null;
        for (Transaction t : transactions)
            if (t.getTransactionID().equals(transactionID))
                res = t;
        return res;
    }

    public Transaction getTransactionFromOrderID(int orderID) {
        Transaction res = null;
        for (Transaction t : transactions)
            if (t.getOrderID() == orderID)
                res = t;
        return res;
    }

    @Override
    public void updateTransaction(Transaction transaction) {
        Transaction t = getTransactionFromID(transaction.getTransactionID());
        t.setStatus(transaction.getStatus());
    }

    @Override
    public void deleteTransaction(Transaction transaction) {
//        ignore
    }

    @Override
    public void addTransaction(Transaction transaction) {
        for (Transaction t : transactions)
            if (t.equals(transaction)) {
                updateTransaction(transaction);
                return;
            }

        transactions.add(transaction);
    }
}
