package com.example.ingsw.ec18_04_m_v001.DBClasses;

public class OrderDetail {

    private int orderId;
    private int productId;
    private String productName;
    private int quantity;
    private double unitCost;
    private double subTotal;

    public OrderDetail(int orderId, int productId, String productName, int quantity, double unitCost) {
        this.orderId = orderId;
        this.productId = productId;
        this.productName = productName;
        this.quantity = quantity;
        this.unitCost = unitCost;
        this.subTotal = quantity * unitCost;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(double unitCost) {
        this.unitCost = unitCost;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof OrderDetail))
            return false;

        OrderDetail od = (OrderDetail) obj;
        return orderId == od.orderId && productId == od.productId && productName.equals(od.productName);
    }
}
