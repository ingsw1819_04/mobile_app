package com.example.ingsw.ec18_04_m_v001.DAOImpl;

import com.example.ingsw.ec18_04_m_v001.DAO.ShoppingCartDAO;
import com.example.ingsw.ec18_04_m_v001.DBClasses.ShoppingCart;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCartDaoImpl implements ShoppingCartDAO {

    private List<ShoppingCart> shoppingcarts = new ArrayList<>();

    @Override
    public List<ShoppingCart> getAllShoppingCarts() {
        return shoppingcarts;
    }

    public ShoppingCart getShoppingcartFromProduct(int productID) {
        ShoppingCart res = null;
        for (ShoppingCart sc : shoppingcarts)
            if (sc.getProductId() == productID)
                res = sc;
        return res;
    }

    @Override
    public void updateShoppingCart(ShoppingCart shoppingcart) {
        for (ShoppingCart sc : shoppingcarts)
            if (sc.equals(shoppingcart)) {
                sc.setQuantity(shoppingcart.getQuantity());
            }
    }

    @Override
    public void deleteShoppingCart(ShoppingCart shoppingcart) {
        shoppingcarts.remove(shoppingcart);
    }

    @Override
    public void addShoppingCart(ShoppingCart shoppingcart) {
        for (ShoppingCart sc : shoppingcarts)
            if (sc.equals(shoppingcart)) {
                int newQuantity = sc.getQuantity() + shoppingcart.getQuantity();
                sc.setQuantity(newQuantity);
                return;
            }

        shoppingcarts.add(shoppingcart);
    }
}
