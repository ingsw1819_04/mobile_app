package com.example.ingsw.ec18_04_m_v001.Activity;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ingsw.ec18_04_m_v001.R;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

public class EasterEggActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_easter_egg);

        Bundle extras = getIntent().getExtras();
        int value = extras.getInt("VALUE");

        YouTubePlayerView youTubePlayerView = findViewById(R.id.youtube_player_view);
        getLifecycle().addObserver(youTubePlayerView);

        youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                String videoId;
                if (value == 3) {
                    videoId = "3fr1Z07AV00";
                } else {
                    videoId = "3n1T3HxHd7Y";
                }
                youTubePlayer.loadVideo(videoId, 0);
            }
        });
    }

    @Override
    public void onBackPressed() {
        //OTHER THINGS
        finish();
    }
}
