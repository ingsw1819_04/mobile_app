package com.example.ingsw.ec18_04_m_v001.Activity;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ingsw.ec18_04_m_v001.DBClasses.Order;
import com.example.ingsw.ec18_04_m_v001.DBClasses.OrderDetail;
import com.example.ingsw.ec18_04_m_v001.R;
import com.example.ingsw.ec18_04_m_v001.RecyclerView.Adapter.OrderDetailAdapter;
import com.example.ingsw.ec18_04_m_v001.RecyclerView.OrderDetailItem;
import com.example.ingsw.ec18_04_m_v001.Utility.Constants;

import java.util.ArrayList;

public class OrderDetailActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter rcAdapter;
    private RecyclerView.LayoutManager rcLayoutManager;
    private ArrayList<OrderDetailItem> orderDetailList = new ArrayList<>();

    // FIXED UI
    private TextView total;
    private TextView timestamp;

    private int orderID;
    private String timeCreation;
    private double shippingCost;
    private double totalCost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        orderID = extras.getInt("ORDER_ID");

        Order selectedOrder = Constants.ORDER.getOrder(orderID);

        timeCreation = "" + selectedOrder.getDateCreate();
        shippingCost = Constants.SHIPPING.getShippinginfo(selectedOrder.getShippingId()).getCost();
        totalCost = selectedOrder.getTotal();

        convertOrderDetailsToOrderItem();

        setContentView(R.layout.activity_order_detail);
        total = findViewById(R.id.total);
        timestamp = findViewById(R.id.timestamp);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        rcLayoutManager = new LinearLayoutManager(this);

        rcAdapter = new OrderDetailAdapter(orderDetailList);
        recyclerView.setLayoutManager(rcLayoutManager);
        recyclerView.setAdapter(rcAdapter);

        timestamp.setText(timeCreation);
        total.setText("" + totalCost);
    }

    /**
     * Create a list for the order details items
     */
    private void convertOrderDetailsToOrderItem() {
        ArrayList<OrderDetail> subList = new ArrayList<>();
        for (OrderDetail od : Constants.ORDER_DETAIL.getAllOrderDetails()) {
            if (od.getOrderId() == orderID) {
                subList.add(od);
            }
        }

        for (OrderDetail od : subList) {
            orderDetailList.add(new OrderDetailItem(od.getQuantity(), od.getProductName(), od.getUnitCost()));
        }

        orderDetailList.add(new OrderDetailItem(1, "Corriere - Spedizione espresso", shippingCost));
    }
}
