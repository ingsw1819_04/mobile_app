package com.example.ingsw.ec18_04_m_v001.DAO;

import com.example.ingsw.ec18_04_m_v001.DBClasses.ProductCategory;

import java.util.List;

public interface ProductCategoryDAO {

    public List<ProductCategory> getAllCategories();

    public void updateCategory(ProductCategory category);

    public void deleteCategory(ProductCategory category);

    public void addCategory(ProductCategory category);
}
