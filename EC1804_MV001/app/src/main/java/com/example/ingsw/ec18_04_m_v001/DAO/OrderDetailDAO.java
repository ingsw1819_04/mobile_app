package com.example.ingsw.ec18_04_m_v001.DAO;

import com.example.ingsw.ec18_04_m_v001.DBClasses.OrderDetail;

import java.util.List;

public interface OrderDetailDAO {

    public List<OrderDetail> getAllOrderDetails();

    public void updateOrderDetail(OrderDetail orderDetail);

    public void deleteOrder(OrderDetail orderDetail);

    public void addOrder(OrderDetail orderDetail);
}
