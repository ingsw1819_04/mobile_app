package com.example.ingsw.ec18_04_m_v001.DAO;

import com.example.ingsw.ec18_04_m_v001.DBClasses.ShippingInfo;

import java.util.List;

public interface ShippingInfoDAO {
    public List<ShippingInfo> getAllShippingInfos();

    public void updateShippingInfo(ShippingInfo shippinginfo);

    public void deleteShippingInfo(ShippingInfo shippinginfo);

    public void addShippingInfo(ShippingInfo shippinginfo);
}
