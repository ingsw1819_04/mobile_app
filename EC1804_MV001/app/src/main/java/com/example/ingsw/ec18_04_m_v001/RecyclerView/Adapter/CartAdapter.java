package com.example.ingsw.ec18_04_m_v001.RecyclerView.Adapter;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ingsw.ec18_04_m_v001.Activity.CartActivity;
import com.example.ingsw.ec18_04_m_v001.DBClasses.CartItem;
import com.example.ingsw.ec18_04_m_v001.R;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

public class CartAdapter extends RecyclerView.Adapter {

    private List<CartItem> cartList;
    private CartViewHolder.OnNoteListner onNoteListner;

    public CartAdapter(List<CartItem> cartList, CartViewHolder.OnNoteListner onNoteListner) {
        this.cartList = cartList;
        this.onNoteListner = onNoteListner;
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_item, parent, false);
        CartViewHolder ovh = new CartViewHolder(v, onNoteListner);
        return ovh;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        CartItem currentItem = cartList.get(i);

        CartViewHolder ovh = (CartViewHolder) viewHolder;
        ovh.price.setText(" € " + currentItem.getUnitCost());
        ovh.name.setText(currentItem.getName());
        ovh.quantity.setText("" + currentItem.getQuantity());

        Blob b = currentItem.getImage();
        byte[] byteArray = null;
        try {
            byteArray = b.getBytes(1, (int) (b.length()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        ovh.productImage.setImageBitmap(bitmap);

        ovh.quantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                currentItem.setQuantity(Integer.valueOf(ovh.quantity.getText().toString()));
                CartActivity.calculateTotal();
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    public static class CartViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView productImage;
        private TextView name;
        private TextView price;
        private TextView quantity;
        private Button decrease;
        private Button increase;
        private OnNoteListner onNoteListner;

        private CartViewHolder(@NonNull View itemView, OnNoteListner onNoteListner) {
            super(itemView);
            productImage = itemView.findViewById(R.id.product_image);
            name = itemView.findViewById(R.id.product_name);
            price = itemView.findViewById(R.id.product_price);
            quantity = itemView.findViewById(R.id.product_quantity);
            decrease = itemView.findViewById(R.id.decrease);
            increase = itemView.findViewById(R.id.increase);
            this.onNoteListner = onNoteListner;

            decrease.setOnClickListener(v -> decrease(Integer.valueOf(quantity.getText().toString())));

            increase.setOnClickListener(v -> increase(Integer.valueOf(quantity.getText().toString())));

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onNoteListner.onNoteClick(getAdapterPosition());
        }

        @SuppressLint("SetTextI18n")
        private void increase(int value) {
            if (value < 10)
                quantity.setText("" + ++value);
            CartActivity.calculateTotal();
        }

        @SuppressLint("SetTextI18n")
        private void decrease(int value) {
            if (value > 0)
                quantity.setText("" + --value);
            CartActivity.calculateTotal();
        }

        public interface OnNoteListner {
            void onNoteClick(int position);
        }
    }
}
