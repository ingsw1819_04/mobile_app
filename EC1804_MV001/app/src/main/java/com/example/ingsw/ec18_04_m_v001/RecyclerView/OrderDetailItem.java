package com.example.ingsw.ec18_04_m_v001.RecyclerView;

public class OrderDetailItem {

    private int quantity;
    private String name;
    private double total;

    public OrderDetailItem(int quantity, String name, double total) {
        this.quantity = quantity;
        this.name = name;
        this.total = total;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
