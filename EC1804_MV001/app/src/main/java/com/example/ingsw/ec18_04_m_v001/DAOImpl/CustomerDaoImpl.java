package com.example.ingsw.ec18_04_m_v001.DAOImpl;

import com.example.ingsw.ec18_04_m_v001.DAO.CustomerDAO;
import com.example.ingsw.ec18_04_m_v001.DBClasses.Customer;

import java.util.ArrayList;
import java.util.List;

public class CustomerDaoImpl implements CustomerDAO {

    private List<Customer> customers = new ArrayList<>();

    @Override
    public List<Customer> getAllCustomers() {
        return customers;
    }

    @Override
    public void updateCustomer(Customer customer) {
        for (Customer c : customers)
            if (c.equals(customer)) {
                c.setName(customer.getName());
                c.setSurname(customer.getSurname());
                c.setDateofbirth(customer.getDateofbirth());
                c.setSex(customer.getSex());
                c.setRegion(customer.getRegion());
                c.setAddress(customer.getAddress());
                c.setCity(customer.getCity());
                c.setCap(customer.getCap());
            }
    }

    public Customer getCustomer(String email) {
        Customer customer = null;
        for (Customer c : customers)
            if (c.getEmail().equals(email))
                customer = c;

        return customer;
    }

    @Override
    public void deleteCustomer(Customer customer) {
//        ignore
    }

    @Override
    public void addCustomer(Customer customer) {
        for (Customer c : customers)
            if (c.equals(customer))
                return;

        customers.add(customer);
    }
}
