package com.example.ingsw.ec18_04_m_v001.Utility.Html;

public final class WelcomeHtml {

    public static final String WELCOME_PAGE = "<!DOCTYPE html>\n" +
            "<html lang='en'>\n" +
            "\t<head>\n" +
            "\t\t<title>ECommerce '18</title>\n" +
            "\t\t<style>\n" +
            "\t\t\t.mail {\n" +
            "\t\t        text-align: center;\n" +
            "\t\t        font-family: 'Times New Roman', Times, serif;\n" +
            "\t\t    }\n" +
            "\t\t\t.header {\n" +
            "\t\t\t    background-color: lightblue;\n" +
            "                margin: auto;\n" +
            "                padding: 2% 0 2% 0;\n" +
            "                border: none;\n" +
            "\t\t\t}\n" +
            "\t\t\t.body {\n" +
            "\t\t\t    margin: 5% 20% 0 20%;\n" +
            "\t\t\t    padding: auto;\n" +
            "\t\t\t}\n" +
            "\t\t    .logo {\n" +
            "\t\t        border: 1px solid;\n" +
            "                border-radius: 5px;\n" +
            "                padding: 5px;\n" +
            "                width: 125px;\n" +
            "\t\t    }\n" +
            "\t\t</style>\n" +
            "\t</head>\n" +
            "\t\n" +
            "\t<body class='mail'>\n" +
            "\t\t\n" +
            "\t\t<div class='header'>\n" +
            "\t\t\t<h1>ECommerce '18</h1>\n" +
            "\t\t\t<p>Registrazione effettuata!</p> \n" +
            "\t\t</div>\n" +
            "\t\t\n" +
            "\t\t<div class='body'>\n" +
            "\t\t\t<h3>Benvenuto!</h3>\n" +
            "\t\t\t<p>La tua registrazione &egrave; stata effettuata con successo.</p>\n" +
            "\t\t\t<p>ECommerce '18 &egrave; lieta di darti il benvenuto!</p>\n" +
            "\t\t\t<hr>\n" +
            "\t\t\t<p>Di Lucrezia Roberto</p><p>Marino Eddy Pasquale</p>\n" +
            "\t\t\t<a href='http://www.unina.it/'><img class='logo' src='https://www.unidformazione.com/wp-content/uploads/2018/04/UniNa-universita-napoli-federico-II.jpg' alt='Responsive image'></a>\n" +
            "\t\t</div>\n" +
            "\n" +
            "\t</body>\n" +
            "</html>";

}
