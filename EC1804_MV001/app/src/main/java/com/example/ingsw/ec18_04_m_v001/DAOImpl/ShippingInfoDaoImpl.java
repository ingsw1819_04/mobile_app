package com.example.ingsw.ec18_04_m_v001.DAOImpl;

import com.example.ingsw.ec18_04_m_v001.DAO.ShippingInfoDAO;
import com.example.ingsw.ec18_04_m_v001.DBClasses.ShippingInfo;

import java.util.ArrayList;
import java.util.List;

public class ShippingInfoDaoImpl implements ShippingInfoDAO {

    private List<ShippingInfo> shippinginfos = new ArrayList<>();

    @Override
    public List<ShippingInfo> getAllShippingInfos() {
        return shippinginfos;
    }

    public ShippingInfo getShippinginfo(int id) {
        ShippingInfo si = null;
        for (ShippingInfo aux : shippinginfos)
            if (aux.getId() == id)
                si = aux;
        return si;
    }

    public ShippingInfo getShippinginfo(String region) {
        ShippingInfo si = null;
        for (ShippingInfo aux : shippinginfos)
            if (aux.getRegionName().equals(region))
                si = aux;
        return si;
    }

    @Override
    public void updateShippingInfo(ShippingInfo shippinginfo) {
        for (ShippingInfo si : shippinginfos)
            if (si.equals(shippinginfo)) {
                si.setType(shippinginfo.getType());
                si.setCost(shippinginfo.getCost());
            }
    }

    @Override
    public void deleteShippingInfo(ShippingInfo shippinginfo) {
//        ignore
    }

    @Override
    public void addShippingInfo(ShippingInfo shippinginfo) {
        for (ShippingInfo si : shippinginfos)
            if (si.equals(shippinginfo))
                return;

        shippinginfos.add(shippinginfo);
    }
}
