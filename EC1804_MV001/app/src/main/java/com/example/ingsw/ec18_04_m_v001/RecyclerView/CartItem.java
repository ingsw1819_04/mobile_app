package com.example.ingsw.ec18_04_m_v001.RecyclerView;

import java.sql.Blob;

public class CartItem {

    private String name;
    private double price;
    private int quantity;
    private Blob image;

    public CartItem(String name, double price, int quantity, Blob image) {
        this.image = image;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Blob getImage() {
        return image;
    }

    public void setImage(Blob image) {
        this.image = image;
    }
}
