package com.example.ingsw.ec18_04_m_v001.RecyclerView.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ingsw.ec18_04_m_v001.R;
import com.example.ingsw.ec18_04_m_v001.RecyclerView.OrderDetailItem;

import java.util.ArrayList;

public class OrderDetailAdapter extends RecyclerView.Adapter {

    private ArrayList<OrderDetailItem> orderDetailList;

    public OrderDetailAdapter(ArrayList<OrderDetailItem> orderDetailList) {
        this.orderDetailList = orderDetailList;
    }

    @NonNull
    @Override
    public OrderDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_detail_item, parent, false);
        OrderDetailViewHolder ovh = new OrderDetailViewHolder(v);
        return ovh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        OrderDetailItem currentItem = orderDetailList.get(i);

        OrderDetailViewHolder ovh = (OrderDetailViewHolder) viewHolder;
        ovh.total.setText("€ " + String.format("%.2f", currentItem.getTotal()));
        ovh.name.setText(currentItem.getName());
        ovh.quantity.setText("x" + currentItem.getQuantity());
    }

    @Override
    public int getItemCount() {
        return orderDetailList.size();
    }

    public static class OrderDetailViewHolder extends RecyclerView.ViewHolder {

        public TextView quantity;
        public TextView name;
        public TextView total;

        public OrderDetailViewHolder(@NonNull View itemView) {
            super(itemView);
            quantity = itemView.findViewById(R.id.quantity);
            name = itemView.findViewById(R.id.product_name);
            total = itemView.findViewById(R.id.total);
        }
    }
}
