package com.example.ingsw.ec18_04_m_v001.DAO;

import com.example.ingsw.ec18_04_m_v001.DBClasses.ShoppingCart;

import java.util.List;

public interface ShoppingCartDAO {
    public List<ShoppingCart> getAllShoppingCarts();

    public void updateShoppingCart(ShoppingCart shoppingcart);

    public void deleteShoppingCart(ShoppingCart shoppingcart);

    public void addShoppingCart(ShoppingCart shoppingcart);
}
