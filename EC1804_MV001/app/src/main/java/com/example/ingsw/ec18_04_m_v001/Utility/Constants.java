package com.example.ingsw.ec18_04_m_v001.Utility;

import com.example.ingsw.ec18_04_m_v001.DAOImpl.CartItemDaoImpl;
import com.example.ingsw.ec18_04_m_v001.DAOImpl.CustomerDaoImpl;
import com.example.ingsw.ec18_04_m_v001.DAOImpl.OrderDaoImpl;
import com.example.ingsw.ec18_04_m_v001.DAOImpl.OrderDetailDaoImpl;
import com.example.ingsw.ec18_04_m_v001.DAOImpl.ProductCategoryDaoImpl;
import com.example.ingsw.ec18_04_m_v001.DAOImpl.ProductDaoImpl;
import com.example.ingsw.ec18_04_m_v001.DAOImpl.ShippingInfoDaoImpl;
import com.example.ingsw.ec18_04_m_v001.DAOImpl.ShoppingCartDaoImpl;
import com.example.ingsw.ec18_04_m_v001.DAOImpl.TransactionDaoImpl;
import com.example.ingsw.ec18_04_m_v001.DAOImpl.UserDaoImpl;

public final class Constants {

    public static final String URL = "jdbc:mariadb://dbprogetto.synology.me:3307/INGSW";
    public static final String USERNAME = "eddy";
    public static final String PASSWORD = "progettoPassword";
    public static final String ALPHANUMBERICSTRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            + "0123456789"
            + "abcdefghijklmnopqrstuvxyz";
    public static final int DIMENSION = 12;
    public static final String PREF_USER_NAME = "username";
    public static final UserDaoImpl USER = new UserDaoImpl();
    public static final ProductDaoImpl PRODUCT = new ProductDaoImpl();
    public static final CustomerDaoImpl CUSTOMER = new CustomerDaoImpl();
    public static final OrderDaoImpl ORDER = new OrderDaoImpl();
    public static final OrderDetailDaoImpl ORDER_DETAIL = new OrderDetailDaoImpl();
    public static final ShippingInfoDaoImpl SHIPPING = new ShippingInfoDaoImpl();
    public static final CartItemDaoImpl CART_ITEM = new CartItemDaoImpl();
    public static final ShoppingCartDaoImpl SHOPPING_CART = new ShoppingCartDaoImpl();
    public static final ProductCategoryDaoImpl CATEGORY = new ProductCategoryDaoImpl();
    public static final TransactionDaoImpl TRANSACTION = new TransactionDaoImpl();
    public static final String PAYPAL_CLIENT_ID = "AYkOnMIzVhByYPucNUrXaFXy55I-cTwl1BitvpkCU9WbsyUssWv_Z_nD5hM3HzsPw3Z0TBDMIn9QRJyy";
    public static String PAYPAL_TRANSATION_ID = "";

    private Constants() {
    }
}