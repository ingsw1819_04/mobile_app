package com.example.ingsw.ec18_04_m_v001.Activity;

import android.annotation.SuppressLint;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ingsw.ec18_04_m_v001.Connection.DBConnection;
import com.example.ingsw.ec18_04_m_v001.DBClasses.Product;
import com.example.ingsw.ec18_04_m_v001.DBClasses.ShoppingCart;
import com.example.ingsw.ec18_04_m_v001.DBClasses.User;
import com.example.ingsw.ec18_04_m_v001.Encoding.SHAEncryption;
import com.example.ingsw.ec18_04_m_v001.R;
import com.example.ingsw.ec18_04_m_v001.Utility.CRUDClass;
import com.example.ingsw.ec18_04_m_v001.Utility.Constants;
import com.example.ingsw.ec18_04_m_v001.Utility.PreferenceData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    private static boolean foundEmail = false;

    private UserLoginTask mAuthTask = null;
    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private TextView mForgPassword;
    private ProgressBar loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = findViewById(R.id.email);

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                String email = mEmailView.getText().toString();
                if (!isEmailValid(email)) {
                    mEmailView.setError(getString(R.string.error_invalid_email));
                } else {
                    mEmailView.setError(null);
                }
                CheckEmailExistence checkEmailExistence = new CheckEmailExistence();
                checkEmailExistence.execute("SELECT USERNAME FROM Users WHERE USERNAME = ?", email);
            }
        };
        mEmailView.addTextChangedListener(afterTextChangedListener);

        mForgPassword = findViewById(R.id.forgottenPassword);
        mPasswordView = findViewById(R.id.password);

        mForgPassword.setOnClickListener(v -> {
            // TODO
            Intent intentRecoverPasswordActivity = new Intent(getApplicationContext(), RecoverPasswordActivity.class);
            startActivity(intentRecoverPasswordActivity);
        });

        final Button mEmailLogInButton = findViewById(R.id.email_sign_in_button);
        loading = findViewById(R.id.login_progress);

        mEmailLogInButton.setOnClickListener(view -> {
            if (foundEmail) {
                mEmailView.setError(null);
                if (isPasswordValid(mPasswordView.getText().toString())) {
                    loading.setVisibility(View.VISIBLE);
                    attemptLogin();
                } else {
                    mPasswordView.setError("Password corta >5");
                }
            } else {
                mEmailView.setError("Email non presente");
                Toast.makeText(this, "Email non presente", Toast.LENGTH_LONG).show();
            }
        });

        TextView mRegistration = findViewById(R.id.registration);
        mRegistration.setOnClickListener(view -> {
            Intent intentRegistrationActivity = new Intent(getApplicationContext(), RegistrationActivity.class);
            startActivity(intentRegistrationActivity);
            setResult(1);
            finish();
        });
    }

    @Override
    public void onBackPressed() {
        setResult(-1);
        finish();
    }

    /**
     * Attempts to sign in the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        mAuthTask = new UserLoginTask(email, password);
        mAuthTask.execute((Void) null);
        Constants.USER.deleteUser(new User(0, "guest@ecommerce.it", null, 0, null));
        RetrieveLoggedUser retrieveLoggedUser = new RetrieveLoggedUser();
        retrieveLoggedUser.execute();
        CRUDClass crudClass = new CRUDClass(this);
        crudClass.retrieveLoggedUserData();
        new android.os.Handler().postDelayed(() -> {
            for (ShoppingCart sc : Constants.SHOPPING_CART.getAllShoppingCarts()) {
                Product product = Constants.PRODUCT.getProduct(sc.getProductId());
                crudClass.updateShoppingCart(product.getId(), sc.getQuantity(), Constants.USER.getActualUser().getId(), 1, false);
            }
        }, 200);
        new android.os.Handler().postDelayed(() -> crudClass.retrieveShoppingCart(), 300);
    }

    /**
     * Check that the e-mail is valid
     *
     * @param email The email to check
     * @return The validity of the verification
     */
    private boolean isEmailValid(String email) {
        return (!email.isEmpty() && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    /**
     * Check that the password is valid
     *
     * @param password The password to check
     * @return The validity of the verification
     */
    private boolean isPasswordValid(String password) {
        return password.length() > 5;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        // ignore
    }

    /**
     * Create adapter to tell the AutoCompleteTextView what to show in its dropdown list
     *
     * @param emailAddressCollection List of addresses to be autocomplete
     */
    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
    }

    @SuppressLint("StaticFieldLeak")
    private class CheckEmailExistence extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            int index = 0;
            try {
                Connection connection = DBConnection.getConnection();

                PreparedStatement preparedStatement = connection.prepareStatement(params[0]);
                preparedStatement.setString(1, params[1]);
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.last();
                index = resultSet.getRow();
            } catch (Exception e) {
                e.printStackTrace();
            }
            foundEmail = index > 0;
            return null;
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    @SuppressLint("StaticFieldLeak")
    private class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            int id = 0;
            try {
                String query = "SELECT ID FROM Users WHERE USERNAME = ? AND PASSWORD_HASH = ?";
                Connection connection = DBConnection.getConnection();

                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, mEmail);
                System.out.println(mEmail);
                preparedStatement.setString(2, SHAEncryption.encryptThisString(mPassword));
                System.out.println(mPassword);
                System.out.println(SHAEncryption.encryptThisString(mPassword));
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.last();
                id = resultSet.getRow();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return id > 0;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;

            if (success) {
                PreferenceData.setUserName(LoginActivity.this, mEmail);
                Intent intent = new Intent();
                intent.putExtra("LOGIN_MAIL", mEmail);
                setResult(2, intent);
                Toast.makeText(LoginActivity.this, getString(R.string.successful_login), Toast.LENGTH_SHORT).show();
                finish();
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }
    }

    private class RetrieveLoggedUser extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Connection connection = DBConnection.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT ID, USERNAME, PASSWORD_HASH, REGISTER_DATE " +
                        "FROM Users " +
                        "WHERE USERNAME = ?");
                preparedStatement.setString(1, PreferenceData.getUserName(LoginActivity.this));
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    Constants.USER.addUser(new User(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), 0, resultSet.getTimestamp(4)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            loading.setVisibility(View.GONE);
        }
    }
}

