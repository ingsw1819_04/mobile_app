package com.example.ingsw.ec18_04_m_v001.DBClasses;

import java.sql.Timestamp;

public class Order {

    private int id;
    private Timestamp dateCreate;
    private Timestamp dateShipped;
    private String customerName;
    private int userId;
    private double total;
    private String status;
    private int shippingId;

    public Order(int id, Timestamp dateCreate, Timestamp dateShipped, String customerName, int userId, double total, String status, int shippingId) {
        this.id = id;
        this.dateCreate = dateCreate;
        this.dateShipped = dateShipped;
        this.customerName = customerName;
        this.userId = userId;
        this.total = total;
        this.status = status;
        this.shippingId = shippingId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Timestamp dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Timestamp getDateShipped() {
        return dateShipped;
    }

    public void setDateShipped(Timestamp dateShipped) {
        this.dateShipped = dateShipped;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getShippingId() {
        return shippingId;
    }

    public void setShippingId(int shippingId) {
        this.shippingId = shippingId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof Order))
            return false;

        Order o = (Order) obj;
        return id == o.id && userId == o.userId;
    }
}

