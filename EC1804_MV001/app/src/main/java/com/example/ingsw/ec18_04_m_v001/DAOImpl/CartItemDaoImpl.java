package com.example.ingsw.ec18_04_m_v001.DAOImpl;

import com.example.ingsw.ec18_04_m_v001.DAO.CartItemDAO;
import com.example.ingsw.ec18_04_m_v001.DBClasses.CartItem;

import java.util.ArrayList;
import java.util.List;

public class CartItemDaoImpl implements CartItemDAO {

    private List<CartItem> cartitems = new ArrayList<>();

    @Override
    public List<CartItem> getAllCartItems() {
        return cartitems;
    }

    @Override
    public void updateCartItem(CartItem cartItem) {
        for (CartItem ci : cartitems)
            if (ci.equals(cartItem)) {
                ci.setQuantity(cartItem.getQuantity());
                ci.setUnitCost(cartItem.getUnitCost());
                ci.setImage(cartItem.getImage());
            }
    }

    @Override
    public void deleteCartItem(CartItem cartItem) {
        cartitems.remove(cartItem);
    }

    @Override
    public void addCartItem(CartItem cartItem) {
        for (CartItem ci : cartitems)
            if (ci.equals(cartItem)) {
                ci.setQuantity(cartItem.getQuantity());
                return;
            }

        cartitems.add(cartItem);
    }
}
