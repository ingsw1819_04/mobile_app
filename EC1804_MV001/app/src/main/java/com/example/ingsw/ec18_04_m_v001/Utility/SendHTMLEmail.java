package com.example.ingsw.ec18_04_m_v001.Utility;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import static com.example.ingsw.ec18_04_m_v001.Utility.Constants.ORDER;
import static com.example.ingsw.ec18_04_m_v001.Utility.Html.ForgotPasswordHtml.getForgotPasswordContentMail;
import static com.example.ingsw.ec18_04_m_v001.Utility.Html.OrderPlacedHtml.getOrderPlacedContentMail;
import static com.example.ingsw.ec18_04_m_v001.Utility.Html.RefundHtml.getRepaymentContentMail;
import static com.example.ingsw.ec18_04_m_v001.Utility.Html.WelcomeHtml.WELCOME_PAGE;

public class SendHTMLEmail {

    // Sender's email ID needs to be mentioned
    private static final String from = "ecommerceunina18@gmail.com";
    private static final String password = "(progettoPassword)";
    private static String to = "";
    private static String messageContent = "";
    private static String subject = "";

    /**
     * This method allows you to send an email in the following cases:
     * 0. registration on the platform
     * 1. password recovery
     *
     * @param toEmail             reciver
     * @param messageContentEmail message to send
     * @param choice              type of message to send
     */
    public static void sendEmail(String toEmail, String messageContentEmail, String choice) {
        to = toEmail;
        switch (choice) {
            case "0":
                subject = "Benvenuto su ECommerce '18";
                messageContent = WELCOME_PAGE;
                break;
            case "1":
                subject = "ECommerce '18 - Nuova Password";
                messageContent = getForgotPasswordContentMail(messageContentEmail);
                break;
            case "2":
                subject = "ECommerce '18 - Ordine effettuato";
                messageContent = getOrderPlacedContentMail(ORDER.getAllOrders().get(0).getId());
                break;
            case "3":
                subject = "ECommerce '18 - Ordine cancellato";
                messageContent = getRepaymentContentMail(ORDER.getAllOrders().get(0).getId());
                break;
        }
        prepareEmail();
    }

    /**
     * Initialize all settings to be able to send an email
     */
    private static void prepareEmail() {
        // Assuming you are sending email through gmail
        String host = "smtp.gmail.com";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "587");

        // Get the Session object.
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(from, password);
                    }
                });
        try {
            // Create a default MimeMessage object.
            Message message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));

            // Set Subject: header field
            message.setSubject(subject);

            // Send the actual HTML message, as big as you like
            message.setContent(
                    messageContent,
                    "text/html");

            // Send message
            Transport.send(message);
            System.out.println("Messaggio inviato con successo...");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    //TEST EMAIL
//    public static void main(String[] args) {
//        sendEmail("eddypasquale@gmail.com", RandomPassword.getAlphaNumericString(), "1");
//        sendEmail("eddypasquale@gmail.com", "", "1");
//        sendEmail("eddypasquale@gmail.com", "", "2");
//        sendEmail("eddypasquale@gmail.com", "", "3");
//    }
}